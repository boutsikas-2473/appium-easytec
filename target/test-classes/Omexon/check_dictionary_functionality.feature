@check_dictionary_functionality
  Feature: Check if Employees Dictionary functionality works

    Scenario:

      Given From the start of the test i must log in the Servers SyncServer with 62.225.181.203,3403,false
      When  I will set my data to log in the application with fkrumscheid,123456
      Then  I will click first activity item with text Orders and click the order with id W2305-008
      And   I will find the seconds activity menu item Employees
      Then  I will click on the dictionary icon and set the Texts
      And   I will check if the text is the same