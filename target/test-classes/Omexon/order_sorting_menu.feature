@order_sorting_menu
Feature: order_sorting_menu


  Scenario Outline:  Check the orders sorting menu works fine

    Given  I have to sign in the app Server <ip>,<port>,<ssl>
    When   I have been successfully get in as a valid user with <user>,<password>
    And    I have to press the order option from the menu
    And    I click on the sorting menu with chosing all options in the list






    Examples:
      | ip | port | ssl | user | password | order | quantity |
      |62.225.181.203 | 3403 | false | fkrumscheid | 123456 | W2305-008 |  10 |
