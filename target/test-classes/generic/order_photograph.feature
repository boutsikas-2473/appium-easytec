Feature: Order photograph

    Background:
    Given I am logged in as "Frank Krumscheid"


    Scenario: Sort order's photographs by Picture Name and ascending mode
      And  Navigate to order ET-1002
      And  Go to Photographs
      And  Sort  by Picture Name in ascending order
      Then Materials are sorted by Picture Name in ascending order

    Scenario: Sort order's photographs by Picture Name and descending mode
     And  Navigate to order ET-1002
     And  Go to Photographs
     And  Sort  by Picture Name in descending order
     Then Materials are sorted by Picture Name in descending order

    Scenario: Sort order's photographs by Date and ascending mode
      And  Navigate to order ET-1002
      And  Go to Photographs
      And Sort  by Date in ascending order
      Then Materials are sorted by Date in ascending order


   Scenario: Sort order's photographs by Date and descending mode
    And  Navigate to order ET-1002
    And  Go to Photographs
    And Sort  by Date in descending order
    Then Materials are sorted by Date in descending order