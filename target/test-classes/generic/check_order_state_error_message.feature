@check_order_state_error_message
Feature: check_order_state_error_message test


  Scenario Outline:  Check after fill all checklist and finish the order without set order state value , if it throws error message when Sync

    Given  I have to sign-in to the apps Sync Server with credentials like<ip>,<port>,<ssl>
    When   I will add as a valid User i have to  successfully sign-in <user>,<password>
    And    I have to  find and   press the Orders menu button
    And    I will click on the in the order with Order_no<order>
    And    I will click on the Checklist menu_options
    And    I will click on the first object
    And    I will click on the first sub_items , set the values and save it
    And    I will  click on the Signatures Menu_options and i will sign all the signatures tab
    And    I will click on the <Menu_Item> and fill the data
    Then   I will go again in the Checklist and i will check if error message appears


    Examples:
      | ip | port | ssl | user | password | order |  Menu_Item |
      |62.225.181.203 | 3403 | false | fkrumscheid | 123456 | W2305-008 |  Employees  |