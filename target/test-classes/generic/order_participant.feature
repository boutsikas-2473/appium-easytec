Feature: Order participant

  Background:
    Given I am logged in as "Frank Krumscheid"

  Scenario: Save start and finish time, message and executed text of order participant
    When Navigate to order ET-1002
    And Go to Employees
    And Select participant employee "F. Krumscheid"
    And Set participant Start Date 07/06/2017 and Time 10:00
    And Set participant Finish Date to 10/06/2017 and Time 11:00
    And Set info This is the message
    And Set executed text This is the executed text
    And Save
    And Navigate to main menu
    And Synchronize
    And Navigate to order ET-1002
    And Go to Employees
    And Check  employee F. Krumscheid
    And Check Start Date  07/06/2017 10:00
    And Check Finish Date to 10/06/2017 11:00
    And Check info This is the message
    And Check Executed text This is the executed text
