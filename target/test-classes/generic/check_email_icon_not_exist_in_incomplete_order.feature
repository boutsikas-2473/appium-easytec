@check_email_icon_not_exist_in_incomplete_order
Feature: check_email_icon_not_exist_in_incomplete_order test


      Scenario Outline:  Check if in  incomplete order , in Report menu option  the email icon exist

        Given  I first must sign in the application syncServer with <ip>,<port>,<ssl>
        When   I will have log_in the system as a valid user with <user>,<password>
        And    I will find the menu item button , order and click it
        And    I will find a incomplete order with id <order>
        And    I will click in the Report Menu option
        Then   I will check if the Email Icon doesn't exist



        Examples:
          | ip | port | ssl | user | password | order |
          |62.225.181.203 | 3403 | false | fkrumscheid | 123456 | W2305-008 |