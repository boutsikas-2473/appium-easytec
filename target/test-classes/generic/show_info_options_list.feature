@show_info_options_list
Feature: show_info_options_list test



  Scenario Outline: Check if orders info sorting menu button works

    Given  I am going to connect into app  SyncServers <ip>,<port>,<ssl>
    When   I will sign in as a valid user with details <user>,<password>
    And    I will Find and Click the order menu option button
    Then   I will check if all options  in info menu options works

    Examples:
      | ip             | port | ssl   | user   | password |
      | 62.225.181.203 | 3403 | false | dohlig | 123456   |
