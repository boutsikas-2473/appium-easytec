Feature: driving_option_txt_changes test

  Background:
    Given I am logged in as "Frank Krumscheid"


  Scenario: The (F) prefix appears at the title while the device owner is driving
    Given  Navigate to main menu
    And    Select employee F. Krumscheid
    When   Pressing Start driving
    Then   Navigate to Orders
    Then   The (F) text exist
    And    Navigate to main menu
    And    Pressing Stop driving
    Then   Navigate to Orders
    Then   The (F) text not exist



  Scenario:  The (P) prefix appears at the title while the device owner is resting
    Given  Navigate to main menu
    And    Select employee F. Krumscheid
    When   Pressing Start resting
    Then   Navigate to Orders
    Then   The (P) text exist
    And    Navigate to main menu
    And    Pressing Stop resting
    And    Navigate to Orders
    Then   The (P) text not exist


  Scenario:The (F) prefix appears at the title while a participant that is not device owner is driving and the device owner is not driving
    Given  Navigate to main menu
    Given  F. Krumscheid is not driving
    Then   Select employee D. Ohlig
    And    Pressing Start driving
    And    Navigate to Orders
    And    The (F) text not exist
    And    Navigate to main menu
    And    Select employee D. Ohlig
    And    Pressing Stop driving
    Then   Navigate to Orders
    Then   The (F) text not exist



  Scenario: The (P) prefix appears at the title while a participant that is not device owner is resting and the device owner is not resting
    Given  Navigate to main menu
    Given  F. Krumscheid is not resting
    Then   Select employee D. Ohlig
    And    Pressing Start resting
    And    Navigate to Orders
    And    The (P) text not exist
    And    Navigate to main menu
    And    Select employee D. Ohlig
    And    Pressing Stop resting
    Then   Navigate to Orders
    Then   The (P) text not exist
