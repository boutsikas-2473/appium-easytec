Feature:  Order Salary

  Background:
  Given I am logged in as "Frank Krumscheid"

     Scenario: Re enter hour value and check it
       When Navigate to order ET-1002
       And Go to Hours and Costs
       And Add an hour
       And Select employee F. Krumscheid
       And Set info This is information
       And Select salary type SalaryCode-U0-WT0
       And Set hours 5
       And Set hours ,
       Then Check if the value is space

  Scenario: Add and synchronize a salary (unit = 0 and working_time = 0)
    When Navigate to order ET-1002
    And Go to Hours and Costs
    And Add an hour
    And Select employee F. Krumscheid
    And Set info This is information
    And Select salary type SalaryCode-U0-WT0
    And Set hours 5
    And Save the salary
    And Navigate to main menu
    And Synchronize
    And Navigate to order ET-1002
    And Go to Hours and Costs
    Then Salary with salary type SalaryCode-U0-WT0, info This is information, hours 5 for employee F. Krumscheid is locked

  Scenario:  Add and synchronize a salary (unit = 0 and working_time = 1)
    When Navigate to order ET-1002
    And Go to Hours and Costs
    And Add an hour
    And Select employee F. Krumscheid
    And Set info This is information
    And Select salary type SalaryCode-U0-WT1
    And Set Start Date 07/06/2017 and Time 10:00
    And Set Finish Date to 10/06/2017 and Time 11:30
    And Save the salary
    And Navigate to main menu
    And Synchronize
    And Navigate to order ET-1002
    And Go to Hours and Costs
    Then Salary with salary type SalaryCode-U0-WT1, info This is information, Start Date 07/06/2017 10:00, Finish Date to 10/06/2017 11:30, for employee F. Krumscheid is locked

  Scenario:  Add and synchronize a salary (unit = 0 and working_time = 2)
    When Navigate to order ET-1002
    And Go to Hours and Costs
    And Add an hour
    And Select employee F. Krumscheid
    And Set info This is information
    And Select salary type SalaryCode-U0-WT2
    And Set Start Date 07/06/2017 and Time 09:00
    And Set Finish Date to 10/06/2017 and Time 11:30
    And Set hours 5
    And Save the salary
    And Navigate to main menu
    And Synchronize
    And Navigate to order ET-1002
    And Go to Hours and Costs
    Then Salary with salary  SalaryCode-U0-WT2, info This is information, Start Date 07/06/2017 09:00, Finish Date to 10/06/2017 11:30, amount 5  ,hours 5 for employee F. Krumscheid is locked

 Scenario:  Add and synchronize a salary (unit = 3)
    When Navigate to order ET-1002
    And Go to Hours and Costs
    And Add an hour
    And Select employee F. Krumscheid
    And Set info This is information
    And Select salary type SalaryCode-U3
    And Set days  5
    And Save the salary
    And Navigate to main menu
    And Synchronize
    And Navigate to order ET-1002
    And Go to Hours and Costs
    Then Salary with salary type SalaryCode-U3, info This is information, and days 5 for employee F. Krumscheid is locked

  Scenario:  Add and synchronize a salary (unit = 4)
    When Navigate to order ET-1002
    And Go to Hours and Costs
    And Add an hour
    And Select employee F. Krumscheid
    And Set info This is information
    And Select salary type SalaryCode-U4
    And Set Start date 07/06/2017
    And Set Finish date 10/06/2017
    And Save the salary
    And Navigate to main menu
   And Synchronize
    And Navigate to order ET-1002
    And Go to Hours and Costs
    Then Salary with salary type SalaryCode-U4, info This is information, Start Date 07/06/2017, Finish Date to 10/06/2017, for employee F. Krumscheid is locked

  Scenario: Fail to add a salary with invalid finish time
    When Navigate to order ET-1002
    And Go to Hours and Costs
    And Add an hour
    And Select salary type SalaryCode-U0-WT1
    And Set Start Date 07/06/2017 and Time 09:00
    And Set Finish Date to 06/06/2017 and Time 11:30
    And Press  save button
    Then Salary is invalid

  Scenario:
    When Navigate to order ET-1002
    And Go to Hours and Costs
    And Add an hour
    And Select employee F. Krumscheid
    And Set info This is information
    And Select salary type SalaryCode-U0-WT0
    And Set hours ,
    And Press  save button
    Then Salary is invalid

    Scenario: Check special character visibility after save
      When Navigate to order ET-1002
      And Go to Hours and Costs
      And Add an hour
      And Select employee F. Krumscheid
      And Set info äö//___///\n
      And Select salary type SalaryCode-U0-WT0
      And Set hours 10
      And Save the salary
      Then Salary with salary type SalaryCode-U0-WT0, info äö//___///\n, hours 10 for employee F. Krumscheid is locked