@check_wrong_version_message

Feature: check_wrong_version_message test


  Scenario Outline:  Check if giving another version connecting details if throws proper message

    Given  I will log in the  Syncs_Servers with false details <ip>,<port>,<ssl>
    When   I am going to  sign in as a valid user  with correct credentials  <user>,<password>
    Then   I must see an error with id <error_id>

    Examples:
      | ip             | port | ssl   | user   | password | error_id     |
      | 62.225.181.203 | 3103 | false | dohlig | 123456   | 149371610335 |
