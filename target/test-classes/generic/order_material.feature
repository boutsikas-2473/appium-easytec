 Feature: Order material

   Background:
        Given I am logged in as "Frank Krumscheid"

  Scenario: Add and synchronize a material

        When Navigate to order ET-1002
        And Go to Materials
        And Add a material with itemNo SEKLER60 and quantity 4
        And Navigate to main menu
        And Synchronize
        And Navigate to order ET-1002
        And Go to materials
        Then Material with itemNo SEKLER60 and quantity 4 is locked

  Scenario: Sort materials by Article No. in ascending order

        When Navigate to main menu
        And  Go to Materials
        And  Sort  by Article No. in ascending order
        Then Materials are sorted by Article No. in ascending order


  Scenario: Sort materials by Article No. in descending order

        When Navigate to main menu
        And  Go to Materials
        And  Sort  by Article No. in descending order
        Then Materials are sorted by Article No. in descending order


  Scenario: Sort materials by Description in ascending order

        When Navigate to main menu
        And  Go to Materials
        And  Sort  by Description in ascending order
        Then Materials are sorted by Description in ascending order


  Scenario: Sort materials by Description in descending order

        When Navigate to main menu
        And  Go to Materials
        And  Sort  by Description in descending order
        Then Materials are sorted by Description in descending order


