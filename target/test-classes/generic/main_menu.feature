Feature:  Main menu

  Scenario:  The items of the main menu are Orders, Materials, Hours Overview and Settings

    When  Navigate to main menu
    Then  I see menu item Orders
    Then  I see menu item Materials
    Then  I see menu item Hours Overview
    Then  I see menu item Settings
    Then  I do not see menu item Start driving
    Then  I do not see menu item Stop driving
    Then  I do not see menu item Start resting
    Then  I do not see menu item Stop resting
    Then  I do not see menu item Employees
    Then  I do not see menu item Equipment
    Then  I do not see menu item Daily report
