Feature: order_measurement

  Background:
    Given I am logged in as "Frank Krumscheid"


  Scenario:
      Given Navigate to order WD-1606
      Then  Go to Measurement Points
      And   Add Measurement point
      And   Set Measurement name testing measurement
      And   Set info this is testing measurement point
      And   Select moisture type 101 - Raumtemp.
      And   Select place type testing
      And   Save
      And   Click on  measurement name testing
      And   Set date  07/06/2017
      And   Set moisture percentanse 5
      And   Set temperature 5
      And   Set air speed 5
      And   Set digits 5
      And   Set SKT 5
      And   Click finish checkbox
      And   Navigate to main menu
      And   Synchronize
      And   Navigate to order WD-1606
      And   Go to Measurement Points
      Then  Check if finish flag exist
      Then I will sync and check if the lock icon exist and the values are the same 60000416-012





