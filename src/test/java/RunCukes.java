import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by tesse on 6/8/2017.
 */
@RunWith(Cucumber.class)
@CucumberOptions(features = {"/home/anna/Development/easytec-tests-appium/src/test/resources/generic/about.feature"})
public class RunCukes {
}
