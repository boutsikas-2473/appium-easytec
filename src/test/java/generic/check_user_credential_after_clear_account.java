package generic;

import TEST_CONFIGURATION_FILES.AndroidSetup;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.android.AndroidDriver;
import org.apache.http.util.TextUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.openqa.selenium.By;

/**
 * Created by tesse on 4/20/2017.
 */
public class check_user_credential_after_clear_account {

    private static AndroidDriver driver;


    //******************* Setting the driver and the capabilities
    @Before
    public void before() {
        try {
            AndroidSetup.deviceCapabilities();
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver = AndroidSetup.getDriver();


    }



    @Given("^I will sync into the applications SyncServer with (.*),(.*),(.*)$")
    public void I_will_sync_into_the_applications_SyncServer_with(String ip, String port, String ssl) {


        driver.findElement(By.xpath("//android.widget.TextView[@text='Settings']")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Account']")).click();

        // set SyncServer IP
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "serverAddressEditTxt")).sendKeys(ip);

        // set SyncServer port
        if (!TextUtils.isEmpty(port)) {
            driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "serverPortEditTxt")).sendKeys(port);
        }

        // enable or disable SSL
        if (Boolean.parseBoolean(ssl)) {
            driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "sslChk")).click();
        }

        // save synchronization settings
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "save")).click();

        driver.findElement(By.id("android:id/button1")).click();


        driver.findElement(By.id("gr.tessera.easytec:id/clearCredentialsBtn")).click();


        driver.findElement(By.id("android:id/button1")).click();

        // we are the main menu
    }



    @When("^I will have successful login as a valid user with (.*),(.*)$")
    public void I_will_have_successful_login_as_a_valid_user_with(String username, String password) throws Throwable {

        // click synchronization action bar button
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "sync")).click();

        // set username
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "usernameEditTxt")).sendKeys(username);
        // set password
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "passwordEditTxt")).sendKeys(password);
        // synchronize (click OK)

        driver.findElement(By.id("android:id/button1")).click();


        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                driver.findElement(By.id("android:id/button2")).click();
            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }

        }

    }


    @And("^I will click on the Setting menu list button$")
    public void I_will_click_on_the_Setting_menu_list_button(){

        driver.findElement(By.xpath("//android.widget.TextView[@text='Settings']")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Account']")).click();

    }


    @And("^I will click on the Clear account data button$")
    public void I_will_click_on_the_Clear_account_data_button(){

        driver.findElement(By.id("gr.tessera.easytec:id/clearCredentialsBtn")).click();

        driver.findElement(By.id("android:id/button1")).click();

    }




    @Then("^I will check if in sync window , shows only the user(.*)$")
    public void I_will_check_if_in_sync_window_shows_only_the_user(String Username){


        // click synchronization action bar button
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "sync")).click();


        // get and test the  username
        Assert.assertEquals("Is not the same",Username , driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "usernameEditTxt")).getText());

        // set password
        Assert.assertEquals("Is not the same","" , driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "passwordEditTxt")).getText());



    }


    @AfterClass
    public static void teardown() throws Exception {
        //driver.quit();
        AndroidSetup.tearDown();
        System.out.println("Ran the after");
    }









}
