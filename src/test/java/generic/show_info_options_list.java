package generic;

import TEST_CONFIGURATION_FILES.AndroidSetup;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import io.appium.java_client.android.AndroidDriver;
import org.apache.http.util.TextUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tesse on 5/2/2017.
 */
public class show_info_options_list {
    private static AndroidDriver driver;
    private ArrayList<String> MenuOptions  = new ArrayList();
    private int menuOptionCounter=0;










    //******************* Setting the driver and the capabilities
    @Before
    public void before() {
        try {

            AndroidSetup.deviceCapabilities();
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver = AndroidSetup.getDriver();


    }



    @Given("^I am going to connect into app  SyncServers (.*),(.*),(.*)$")
    public void I_am_going_to_connect_into_app_SyncServers(String ip, String port, String ssl) {


        driver.findElement(By.xpath("//android.widget.TextView[@text='Settings']")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Account']")).click();

        // set SyncServer IP
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "serverAddressEditTxt")).sendKeys(ip);

        // set SyncServer port
        if (!TextUtils.isEmpty(port)) {
            driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "serverPortEditTxt")).sendKeys(port);
        }

        // enable or disable SSL
        if (Boolean.parseBoolean(ssl)) {
            driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "sslChk")).click();
        }

        // save synchronization settings
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "save")).click();

        driver.findElement(By.id("android:id/button1")).click();


        driver.findElement(By.id("gr.tessera.easytec:id/clearCredentialsBtn")).click();


        driver.findElement(By.id("android:id/button1")).click();

        // we are the main menu
    }



    @When("^I will sign in as a valid user with details (.*),(.*)$")
    public void I_will_sign_in_as_a_valid_user_with_details(String username, String password) throws Throwable {

        // click synchronization action bar button
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "sync")).click();

        // set username
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "usernameEditTxt")).sendKeys(username);
        // set password
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "passwordEditTxt")).sendKeys(password);
        // synchronize (click OK)

        driver.findElement(By.id("android:id/button1")).click();


        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                driver.findElement(By.id("android:id/button2")).click();
            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }

        }

    }



    @And("^I will Find and Click the order menu option button")
    public void I_will_Find_and_Click_the_order_menu_option_button() throws Throwable {
        // //*[contains(@text, 'Sign Out')]

        driver.findElement(By.xpath("//android.widget.TextView[@text='Orders']")).click();
    }



    @And("^I will check if all options  in info menu options works$")
    public void I_will_check_if_all_options_in_info_menu_options_works() throws InterruptedException, ParseException {



        // get the order list layout before we start
        WebElement OrdersListView = driver.findElement(By.className("android.widget.ListView"));
        List<WebElement> OrderLayoutList = OrdersListView.findElements(By.className("android.widget.LinearLayout"));


        // find the view where the sort button on the menu is
        WebElement View  =  driver.findElement(By.className("android.view.View"));
        List<WebElement> RelativeLayoutsList = View.findElements(By.className("android.widget.FrameLayout"));


        // first erase hte filter panel

        driver.findElement(By.id("gr.tessera.easytec:id/filtersToggleTxt")).click();



        //find the sorting button from the menu

        RelativeLayoutsList.get(0).findElement(By.id("gr.tessera.easytec:id/showInfo")).click();



        Thread.sleep(2000);


        //Get the sorting menu ListView

        WebElement ListView = driver.findElement(By.className("android.widget.ListView"));

        List<WebElement> LinearLayouts = ListView.findElements(By.className("android.widget.LinearLayout"));


        Thread.sleep(2000);


        while(menuOptionCounter!=LinearLayouts.size()) {


            // save the option name in th array

            MenuOptions.add(LinearLayouts.get(menuOptionCounter).findElement(By.className("android.widget.TextView")).getText());

            // click it and wait for action

            System.out.print("Order Names  : " + MenuOptions.get(menuOptionCounter) + "\n");


            LinearLayouts.get(menuOptionCounter).click();

            Thread.sleep(2000);


            if(MenuOptions.get(menuOptionCounter).contains("Start Date")){

                Thread.sleep(2000);

                try{
                Assert.assertFalse(OrderLayoutList.get(0).findElement(By.id("gr.tessera.easytec:id/orderStartDateTxt")).isDisplayed());

                }catch (Exception e){
                    Assert.assertTrue("The Object does not exist",true);
                }

                // increase the counter
                menuOptionCounter++;

                //push the Sort menu button
                RelativeLayoutsList.get(0).findElement(By.id("gr.tessera.easytec:id/showInfo")).click();



            }else if(MenuOptions.get(menuOptionCounter).contains("Finish Date")){

                Thread.sleep(2000);

                // check the assertion

                try{
                    Assert.assertFalse(OrderLayoutList.get(0).findElement(By.id("gr.tessera.easytec:id/orderFinishDateTxt")).isDisplayed());


                }catch (Exception e){
                        Assert.assertTrue("The Object does not exist",true);
                }
                // increase the counter
                menuOptionCounter++;

                //push the Sort menu button
                RelativeLayoutsList.get(0).findElement(By.id("gr.tessera.easytec:id/showInfo")).click();



            }else if(MenuOptions.get(menuOptionCounter).contains("Order description (prj)")){


                Thread.sleep(2000);
                // check the assertion

                try{
                    Assert.assertFalse(OrderLayoutList.get(0).findElement(By.id("gr.tessera.easytec:id/orderDescriptionPrjTxt")).isDisplayed());

                }catch (Exception e){
                    Assert.assertTrue("The Object does not exist",true);
                }
                // increase the counter
                menuOptionCounter++;


                //push the Sort menu button
                RelativeLayoutsList.get(0).findElement(By.id("gr.tessera.easytec:id/showInfo")).click();



            }else{

                // increase the counter
                menuOptionCounter++;

                //push the Sort menu button
                RelativeLayoutsList.get(0).findElement(By.id("gr.tessera.easytec:id/showInfo")).click();
            }

        }





    }


    @AfterClass
    public static void teardown() throws Exception {
        //driver.quit();
        AndroidSetup.tearDown();
        System.out.println("Ran the after");
    }



}
