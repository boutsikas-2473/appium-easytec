package generic;

import TEST_CONFIGURATION_FILES.AndroidSetup;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.SwipeElementDirection;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import org.apache.http.util.TextUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.List;

/**
 * Created by tesse on 5/2/2017.
 */
public class check_order_state_error_message {


    private static AndroidDriver driver;

    //******************* Setting the driver and the capabilities
    @Before
    public void before() {
        try {
            AndroidSetup.deviceCapabilities();
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver = AndroidSetup.getDriver();


    }



    @Given("^I have to sign-in to the apps Sync Server with credentials like(.*),(.*),(.*)$")
    public void I_have_to_sign__in_to_the_apps_Sync_Server_with_credentials_like(String ip, String port, String ssl) {


        driver.findElement(By.xpath("//android.widget.TextView[@text='Settings']")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Account']")).click();

        // set SyncServer IP
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "serverAddressEditTxt")).sendKeys(ip);

        // set SyncServer port
        if (!TextUtils.isEmpty(port)) {
            driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "serverPortEditTxt")).sendKeys(port);
        }

        // enable or disable SSL
        if (Boolean.parseBoolean(ssl)) {
            driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "sslChk")).click();
        }

        // save synchronization settings
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "save")).click();

        driver.findElement(By.id("android:id/button1")).click();


        driver.findElement(By.id("gr.tessera.easytec:id/clearCredentialsBtn")).click();


        driver.findElement(By.id("android:id/button1")).click();

        // we are the main menu
    }



    @When("^I will add as a valid User i have to  successfully sign-in (.*),(.*)$")
    public void I_will_add_as_a_valid_User_i_have_to_successfully_sign__in(String username, String password) throws Throwable {

        // click synchronization action bar button
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "sync")).click();

        // set username
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "usernameEditTxt")).sendKeys(username);
        // set password
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "passwordEditTxt")).sendKeys(password);
        // synchronize (click OK)

        driver.findElement(By.id("android:id/button1")).click();


        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                driver.findElement(By.id("android:id/button2")).click();
            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }

        }

    }



    @And("^I have to  find and   press the Orders menu button$")
    public void I_have_to_find_and_press_the_Orders_menu_button() throws Throwable {
        // //*[contains(@text, 'Sign Out')]

        driver.findElement(By.xpath("//android.widget.TextView[@text='Orders']")).click();
    }


    @And("^I will click on the in the order with Order_no(.*)$")
    public void I_will_click_on_the_in_the_order_with_Order__no(String orderNo) throws Throwable {

        assert driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")) != null;
        if(driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")) != null){
            driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")).click();
        }else{

        }
    }


    @And("^I will click on the Checklist menu_options$")
    public void I_will_click_on_the_Checklist_menu__options() throws InterruptedException {

        Thread.sleep(3000);

        driver.findElement(By.xpath("//android.widget.TextView[@text='Checklists/Signatures']")).click();

    }


    @And("^I will click on the first object$")
    public void I_will_click_on_the_first_object(){


        WebElement ListView  = driver.findElement(By.className("android.widget.ListView"));

        ListView.findElements(By.className("android.widget.LinearLayout")).get(0).click();

    }



    @And("^I will click on the first sub_items , set the values and save it$")
    public void I_will_click_on_the_first_sub_items_set_the_values_and_save_it() throws InterruptedException {


        driver.findElement(By.id("gr.tessera.easytec:id/save")).click();


        Thread.sleep(1000);


    }



    @And("^I will  click on the Signatures Menu_options and i will sign all the signatures tab$")
    public void I_will_click_on_the_Signatures_Menu_options_and_i_will_sign_all_the_signatures_tab() throws InterruptedException {


        driver.findElement(By.id("gr.tessera.easytec:id/signCheckList")).click();


        Thread.sleep(1000);



        //***
        //   Find and click the first tab Process
        //***



        //get touchAction object
        TouchAction touchAction = new TouchAction((MobileDriver)driver);


        //find all the tab objects
        List<WebElement> Tab_menu = driver.findElement(By.className("android.widget.TabWidget")).findElements(By.className("android.widget.LinearLayout"));



        //find the Signature Field
        WebElement SignatureImageView = driver.findElement(By.id("gr.tessera.easytec:id/pcSignatureArea"));

        // find the coordinates of the Signature view
        int x = SignatureImageView.getLocation().getX();

        int y = SignatureImageView.getLocation().getY();


        //set the touch Motion event
        touchAction.longPress(SignatureImageView).moveTo(x+200, y).release();

        Thread.sleep(1000);

        touchAction.perform();



        //****
        //   Click on the second tab
        //****



        // get the tab object
        Tab_menu.get(1).click();

        Thread.sleep(1500);


        //find the Signature Field
        WebElement  SignatureImageView1= driver.findElement(By.id("gr.tessera.easytec:id/poSignatureArea"));


        // find the coordinates of the Signature view
        int x1 = SignatureImageView1.getLocation().getX();
        int y1 = SignatureImageView1.getLocation().getY();

        //get touchAction object
        TouchAction touchAction1 =  new TouchAction((MobileDriver)driver);

        //set the touch Motion event
        touchAction1.longPress(SignatureImageView1).moveTo(x1+200, y1).release();

        Thread.sleep(1000);


        touchAction1.perform();




        //****
        //   Click on the third tab
        //****

        // get the tab object
        Tab_menu.get(2).click();

        Thread.sleep(1500);

        //find the Signature Field
        WebElement SignatureImageView2= driver.findElement(By.id("gr.tessera.easytec:id/ccSignatureArea"));

        // find the coordinates of the Signature view
        int  x2 = SignatureImageView2.getLocation().getX();
        int  y2 = SignatureImageView2.getLocation().getY();

        //get touchAction object
        TouchAction touchAction2 =  new TouchAction((MobileDriver)driver);


        //set the touch Motion event
        touchAction2.longPress(SignatureImageView2).moveTo(x2+200, y2).release();

        Thread.sleep(1000);


        touchAction2.perform();

        Thread.sleep(2000);



        // set a signature name (*** needed ***)
        driver.findElement(By.id("gr.tessera.easytec:id/ccNameSignEditText")).sendKeys("test");
        driver.hideKeyboard();




        //****
        //   Click on the fourth tab
        //****


        // get the tab object
        Tab_menu.get(3).click();


        Thread.sleep(1500);

        //find the Signature Field
        WebElement   SignatureImageView3= driver.findElement(By.id("gr.tessera.easytec:id/coSignatureArea"));

        // find the coordinates of the Signature view
        int x3 = SignatureImageView3.getLocation().getX();
        int y3 = SignatureImageView3.getLocation().getY();

        //get touchAction object
        TouchAction touchAction3 =  new TouchAction((MobileDriver)driver);

        //set the touch Motion event
        touchAction3.longPress(SignatureImageView3).moveTo(x3+200, y3).release();

        touchAction3.perform();

        Thread.sleep(1000);

        // set a signature name (*** needed ***)
        driver.findElement(By.id("gr.tessera.easytec:id/coNameSignEditText")).sendKeys("test");
        driver.hideKeyboard();




        // save the whole process
        driver.findElement(By.id("gr.tessera.easytec:id/save")).click();

        Thread.sleep(1000);

        //go to main menu list
        // save the whole process
        driver.findElement(By.id("android:id/home")).click();

    }


    @And("^I will click on the (.*) and fill the data$")
    public void I_will_click_on_the_and_and_check_if_the_error_message_appears(String Menu_Item) throws InterruptedException {

        //click on the Employees menu item
        driver.findElement(By.xpath("//android.widget.TextView[@text='"+Menu_Item+"']")).click();

        // get  ListView Object

        WebElement ListView = driver.findElement(By.className("android.widget.ListView"));

        //get LinearLayout objects List

        List<WebElement> LinearLayoutList = ListView.findElements(By.className("android.widget.LinearLayout"));

        // click on the Start Date button , to set the start Date

        LinearLayoutList.get(3).findElement(By.className("android.widget.Button")).click();

        Thread.sleep(1000);

        // click oonthe Ok Button

        driver.findElement(By.id("android:id/button1")).click();

        Thread.sleep(1000);

        // click on the finish date , to set the finish Date

        LinearLayoutList.get(4).findElement(By.className("android.widget.Button")).click();


        // find the DatePicker object
        WebElement DatePicker =driver.findElement(By.id("gr.tessera.easytec:id/datePicker"));

        //create the process to change the current date
        WebElement NumberPickerFirstLayout = DatePicker.findElement(By.className("android.widget.LinearLayout"));

        WebElement NumberPickerFirstLayout1 = NumberPickerFirstLayout.findElement(By.className("android.widget.LinearLayout"));

        WebElement NumberPickers = NumberPickerFirstLayout1.findElement(By.className("android.widget.NumberPicker"));

        //create new mobileElement , in order to achieve the swipe action
        MobileElement mobileElement = (MobileElement) NumberPickers;

        //make the action
        mobileElement.swipe(SwipeElementDirection.DOWN, 100);

        Thread.sleep(1000);

        //click on the ok button

        driver.findElement(By.id("android:id/button1")).click();



        //click on the flag button

        driver.findElement(By.id("gr.tessera.easytec:id/finish")).click();



        //get touchAction object
        TouchAction touchAction = new TouchAction((MobileDriver)driver);


        //find the Signature Field
        WebElement SignatureImageView = driver.findElement(By.id("gr.tessera.easytec:id/signatureArea"));

        // find the coordinates of the Signature view
        int x = SignatureImageView.getLocation().getX();

        int y = SignatureImageView.getLocation().getY();


        //set the touch Motion event
        touchAction.longPress(SignatureImageView).moveTo(x+200, y).release();

        Thread.sleep(1000);

        touchAction.perform();

        Thread.sleep(1000);

        //click on the save button

        driver.findElement(By.id("gr.tessera.easytec:id/save")).click();

        //click again on the save button

        Thread.sleep(1000);

        driver.findElement(By.id("gr.tessera.easytec:id/save")).click();

        //go to main panel

        driver.findElement(By.id("android:id/home")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("android:id/home")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("android:id/home")).click();

    }


    @Then("^I will go again in the Checklist and i will check if error message appears$")
    public void I_will_go_again_in_the_Checklist_and_i_will_check_if_error_message_appearss() throws InterruptedException {

      // go to sync and check the error


        Thread.sleep(3000);

        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "sync")).click();

        Thread.sleep(8000);


        Assert.assertTrue(driver.findElement(By.id("android:id/message")).getText().contains("Order state")&&driver.findElement(By.id("android:id/message")).getText().contains("could not be completed"));

    }







    }
