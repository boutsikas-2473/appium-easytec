package generic;

import TEST_CONFIGURATION_FILES.AndroidSetup;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import org.apache.http.util.TextUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by tesse on 4/27/2017.
 */
public class successfully_delete_order {





    private static AndroidDriver driver;

    //******************* Setting the driver and the capabilities
    @Before
    public void before() {
        try {
            AndroidSetup.deviceCapabilities();
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver = AndroidSetup.getDriver();


    }



    @Given("^I have to sign-in to the  Syncs Servers with(.*),(.*),(.*)$")
    public void I_have_to_sign_in_to_the_Syncs_Servers_with(String ip, String port, String ssl) {


        driver.findElement(By.xpath("//android.widget.TextView[@text='Settings']")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Account']")).click();

        // set SyncServer IP
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "serverAddressEditTxt")).sendKeys(ip);

        // set SyncServer port
        if (!TextUtils.isEmpty(port)) {
            driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "serverPortEditTxt")).sendKeys(port);
        }

        // enable or disable SSL
        if (Boolean.parseBoolean(ssl)) {
            driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "sslChk")).click();
        }

        // save synchronization settings
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "save")).click();

        driver.findElement(By.id("android:id/button1")).click();


        driver.findElement(By.id("gr.tessera.easytec:id/clearCredentialsBtn")).click();


        driver.findElement(By.id("android:id/button1")).click();

        // we are the main menu
    }



    @When("^I have to sign-in as a valid User with credentials  (.*),(.*)$")
    public void I_have_to_sign_in_as_a_valid_User_with_credentials(String username, String password) throws Throwable {

        // click synchronization action bar button
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "sync")).click();

        // set username
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "usernameEditTxt")).sendKeys(username);
        // set password
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "passwordEditTxt")).sendKeys(password);
        // synchronize (click OK)

        driver.findElement(By.id("android:id/button1")).click();


        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                driver.findElement(By.id("android:id/button2")).click();
            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }

        }

    }



    @And("^I have to Press the  Orders menu button$")
    public void I_have_to_Press_the_Orders_menu_button() throws Throwable {
        // //*[contains(@text, 'Sign Out')]

        driver.findElement(By.xpath("//android.widget.TextView[@text='Orders']")).click();

        Thread.sleep(1000);


    }


    @And("^I will long press on the order with id (.*)$")
    public void I_will_long_press_on_the_order_with_id(String OrderNo) throws InterruptedException {



        TouchAction mtouchAction = new TouchAction((MobileDriver)driver);

        WebElement ListView  = driver.findElement(By.className("android.widget.ListView"));

        List<WebElement> LinearLayoutList = ListView.findElements(By.className("android.widget.LinearLayout"));

        mtouchAction.longPress(LinearLayoutList.get(0),2000).release();

        mtouchAction.perform();




    }




    @And("^I will click the delete menu button$")
    public void I_will_click_the_delete_menu_button() throws InterruptedException {


        Thread.sleep(1000);

        driver.findElement(By.id("gr.tessera.easytec:id/delete")).click();

        Thread.sleep(1000);

        driver.findElement(By.id("android:id/button1")).click();


        Thread.sleep(1000);




    }


    @Then("^I will check if the order has been deleted$")
    public void I_will_check_if_the_order_has_been_deleted(){

        Assert.assertNotNull(driver.findElement(By.xpath("//android.widget.TextView[@text='0 items found']")));
    }




}
