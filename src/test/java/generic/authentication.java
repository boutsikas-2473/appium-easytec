package generic;

import TEST_CONFIGURATION_FILES.AndroidSetup;
import TEST_CONFIGURATION_FILES.AndroidSetup_OMEXON;
import com.sun.jna.IntegerType;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.apache.http.util.TextUtils;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.Properties;

/**
 * Created by tesse on 5/25/2017.
 */
public class authentication {

    private static AndroidDriver<MobileElement> driver;
    private Properties properties = new Properties();
    //******************* Setting the driver and the capabilities
    @Before
    public void before() {
       
        driver = AndroidSetup.getDriver();


    }




    @Given("^Set The SyncServer Credentials (.*),(.*),(.*)$")
    public void set_the_syncserver_credentials(String ip, String port, String ssl) {

        // set SyncServer IP
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "serverAddressEditTxt")).clear();
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "serverAddressEditTxt")).sendKeys(ip);

        if (!TextUtils.isEmpty(port)) {
            driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "serverPortEditTxt")).clear();
            driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "serverPortEditTxt")).sendKeys(port);
        }

        // enable or disable SSL
        if (Boolean.parseBoolean(ssl)) {
            driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "sslChk")).click();
        }

        // save synchronization settings
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "save")).click();

        driver.findElement(By.id("android:id/button1")).click();

        // we are the main menu
    }



    @And("^Click (.*) button$")
    public void click_clear_account_data_button(String itemText){
        driver.findElementByXPath("//android.widget.Button[contains(@text,'"+itemText+"')]").click();
        driver.findElementByXPath("//android.widget.Button[contains(@text,'OK')]").click();
    }




    @When("^Set the user data credentials (.*),(.*)$")
    public void set_the_user_data_redentials(String username, String password) throws Throwable {

        // click synchronization action bar button
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "sync")).click();

        // set username
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "usernameEditTxt")).sendKeys(username);
        // set password
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "passwordEditTxt")).sendKeys(password);
        // synchronize (click OK)

        driver.findElement(By.id("android:id/button1")).click();


        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                driver.findElement(By.id("android:id/button2")).click();
            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }
        }

    }




    @When("^Set the user  credentials (.*),(.*)$")
    public void set_the_user_redentials(String username, String password) throws Throwable {
         // click synchronization action bar button
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "sync")).click();

        // set username
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "usernameEditTxt")).sendKeys(username);
        // set password
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "passwordEditTxt")).sendKeys(password);
        // synchronize (click OK)

        driver.findElement(By.id("android:id/button1")).click();



    }
}
