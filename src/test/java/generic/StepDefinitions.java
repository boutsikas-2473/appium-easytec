package generic;

import TEST_CONFIGURATION_FILES.AndroidSetup;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import org.apache.http.util.TextUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import static TEST_CONFIGURATION_FILES.AndroidSetup.MAIN_ACTIVITY;
import static org.springframework.jmx.support.MetricType.COUNTER;

public class StepDefinitions {
    private AndroidDriver<MobileElement> driver;
    private ResourceBundle resources;
    private  int index = 0;
    private boolean foundElement=false;
    private static final String ISO8601_DATETIME = "yyyy-MM-dd HH:mm:ss";
    private static final String APP_DATETIME_EN = "dd/MM/yyyy HH:mm";
    private static final String APP_DATE_EN = "dd/MM/yyyy";
    private static final String APP_DATETIME = "__APPDATETIME__";
    private static final String APP_DATE = "__APPDATE__";
    private String amountData=null;
    private int CURRENT_POSITION=0, PAST_POSITION=5, LASTPOSITION=0,COUNTER=0;
    private  WebElement EMPLOYMENT_EL=null, HOURS_AND_COSTS_EL=null , CHECKLIST_EL=null;
    private boolean FOUND_ELEMENT_FLAG =  false;

    @Before
    public void before() {
        resources = ResourceBundle.getBundle("strings");
        driver = AndroidSetup.getDriver();
    }




    @Then("^Check if the text is (.*)")
    public void check_if_the_text_is(String UserName){
        //check if username is the same

        assert driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "usernameEditTxt")).getText().contains(UserName);
        // check is password has gone

    }

    @And("^Check if the password is empty$")
    public void check_if_the_password_isempty(){
        assert driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "passwordEditTxt")).getText().matches("");
    }


    @Then("^check if version is (.*)$")
    public void check_if_version_is(String version){
          assert  driver.findElementByXPath("//android.widget.TextView[contains(@text,'Version')]").getText().contains(version);
    }

    @Then("^check if the version is not (.*)$")
    public void check_if_version_is_not(String version){
        assert  !driver.findElementByXPath("//android.widget.TextView[contains(@text,'Version')]").getText().contains(version);
    }

    @Then("^I have order (.*) in the order list$")
    public void i_have_order_in_the_order_list(String orderNo) throws Throwable {
        assert driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + orderNo + "')]")) != null;
    }

    @When("^Navigate to the order list$")
    public void navigate_to_the_order_list() throws Throwable {
        driver.findElement(By.xpath("//android.widget.TextView[@text='" + resources.getString("orders") + "']")).click();
    }


    @Then("^Display error id (.*)$")
    public void display_error_id_void(String Id){
         assert    driver.findElementById("android:id/alertTitle").getText().contains(Id);
         assert    driver.findElementById("android:id/message").getText().matches(resources.getString("wrong_version_message"));
         driver.findElementByXPath("//android.widget.Button[@text='OK']").click();
    }

    @Given("^I am logged in as (.*)")
    public void i_am_logged_in_as(String username){
        goToMainMenu();

        driver.findElement(By.xpath("//android.widget.TextView[@text='" + resources.getString("settings") + "']")).click();
        driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + resources.getString("about_easytec") + "')]")).click();
        try{
            if(driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.textContains(\"Unknown\"))").isDisplayed()){
                login();
            }

        }catch(Exception e){
            assert   driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.textContains("+ username +"))")!= null;
            goToMainMenu();
        }





        goToMainMenu();
    }


    @And("^Go to (.*)$")
    public void go_to_option(String menuOption){
        goToMenuOption(menuOption);
    }

    @When("Navigate to order (.*)")
    public void navigate_to_order(String orderNo){
        driver.findElement(By.xpath("//android.widget.TextView[@text='Orders']")).click();
        goToOrder(orderNo);
    }

    @And("^Navigate to Orders$")
    public void go_to_Orders(){
        driver.findElement(By.xpath("//android.widget.TextView[@text='Orders']")).click();
    }



    @When("^Navigate to main menu$")
    public void go_to_main_menu() {
        goToMainMenu();
    }

    @When("^Add a material with itemNo (.*) and quantity (.*)$")
    public void add_a_material(String itemNo, String quantity){

        driver.findElement(By.id("gr.tessera.easytec:id/add_material")).click();
        selectMaterial(itemNo);
        driver.findElement(By.id("gr.tessera.easytec:id/amountTxt")).click();

        addAmount(quantity);
        driver.findElement(By.id("gr.tessera.easytec:id/save")).click();
    }

    @Then("^Salary with salary type (.*), info (.*), hours (.*) for employee (.*) is locked$")
    public void salary_with_salary_type_info_hours_for_employee_is_locked(String salaryCode, String info, String hours, String employee) throws Throwable {

        List<MobileElement> salaries = driver.findElement(By.id("gr.tessera.easytec:id/salaryListView")).findElementsById("gr.tessera.easytec:id/salaryCodeFirstlayout");

System.out.println("The screen" + driver.currentActivity());

        do{
            if(salaries.get(index).findElementById("gr.tessera.easytec:id/firstTxt").getText().matches(salaryCode)){
                foundElement=!foundElement;
            }else{
                index++;
            }

        }while(!foundElement);


        MobileElement lastSalary = salaries.get(index);
        assert lastSalary.findElement(By.id("gr.tessera.easytec:id/salaryAmountTxt")).getText().startsWith(hours);
        assert lastSalary.findElement(By.id("gr.tessera.easytec:id/secondTxt")).getText().equals(employee);
        assert lastSalary.findElement(By.id("gr.tessera.easytec:id/firstTxt")).getText().equals(salaryCode);

        lastSalary.click();
        List<MobileElement> fields = driver.findElements(By.id("gr.tessera.easytec:id/valueTxt"));

        assert fields.get(0).getText().equals(employee);
        assert !Boolean.parseBoolean(fields.get(0).getAttribute("clickable"));

        assert fields.get(2).getText().equals(salaryCode);
        assert !Boolean.parseBoolean(fields.get(2).getAttribute("clickable"));

        assert fields.get(3).getText().startsWith(hours);
        assert !Boolean.parseBoolean(fields.get(3).getAttribute("clickable"));

        assert fields.get(4).getText().equals(info);
        assert !Boolean.parseBoolean(fields.get(4).getAttribute("clickable"));

    }





    @Then("^Salary with salary type (.*), info (.*), Start Date (.*), Finish Date to (.*), for employee (.*) is locked$")
    public void salary_with_salary_type_salaryCode__info_this_is_information_start_time_finish_time_amount_48_for_employee_is_locked(String salaryCode,String info,String StartTime,String FinishTime, String employee) throws Throwable {

        List<MobileElement> salaries = driver.findElement(By.id("gr.tessera.easytec:id/salaryListView")).findElementsById("gr.tessera.easytec:id/salaryCodeFirstlayout");

        do{
            if(salaries.get(index).findElementById("gr.tessera.easytec:id/firstTxt").getText().matches(salaryCode)){
                foundElement=!foundElement;
            }else{
                index++;
            }

        }while(!foundElement);


        if(salaryCode.matches("SalaryCode-U4")){
            String[] firsttime = StartTime.split("/") , finishtime = FinishTime.split("/");
            amountData = String.valueOf((Integer.valueOf(finishtime[0])-Integer.valueOf(firsttime[0]))+1);

        }else{
            long startTimeInSecond =  fromDateToTimestamp(formatDate(StartTime,APP_DATETIME,ISO8601_DATETIME),ISO8601_DATETIME);
            long finishTimeInSecond = fromDateToTimestamp(formatDate(FinishTime,APP_DATETIME,ISO8601_DATETIME),ISO8601_DATETIME);
            amountData = String.format(Locale.UK, "%.1f", (double) (finishTimeInSecond-startTimeInSecond) / 3600).replace(".",",");
        }


        MobileElement lastSalary = salaries.get(index);

        assert lastSalary.findElement(By.id("gr.tessera.easytec:id/salaryAmountTxt")).getText().contains(amountData);
        assert lastSalary.findElement(By.id("gr.tessera.easytec:id/secondTxt")).getText().equals(employee);
        assert lastSalary.findElement(By.id("gr.tessera.easytec:id/firstTxt")).getText().equals(salaryCode);

        lastSalary.click();
        List<MobileElement> fields = driver.findElements(By.id("gr.tessera.easytec:id/valueTxt"));

        assert fields.get(0).getText().equals(employee);
        assert !Boolean.parseBoolean(fields.get(0).getAttribute("clickable"));

        assert fields.get(2).getText().startsWith(salaryCode);
        assert !Boolean.parseBoolean(fields.get(2).getAttribute("clickable"));

        assert fields.get(3).getText().contains(StartTime);
        assert !Boolean.parseBoolean(fields.get(3).getAttribute("clickable"));

        assert fields.get(4).getText().contains(FinishTime);
        assert !Boolean.parseBoolean(fields.get(4).getAttribute("clickable"));

        assert fields.get(5).getText().contains(amountData);
        assert !Boolean.parseBoolean(fields.get(5).getAttribute("clickable"));

        assert fields.get(6).getText().equals(info);
        assert !Boolean.parseBoolean(fields.get(6).getAttribute("clickable"));

    }


    //***********************************************************************************************************************/

    @Then("Salary with salary  (.*), info (.*), Start Date (.*), Finish Date to (.*), amount (.*)  ,hours (.*) for employee (.*) is locked")
    public void salar_with_salary_type_salarycode_u0_wt2_info_this_is_information_start_time_finish_amount_hours_5_for_employee_is_locked(String salaryCode, String info,String StartTime ,String FinishTime,String amount,String Hours, String employee) throws ParseException {

        List<MobileElement> salaries = driver.findElement(By.id("gr.tessera.easytec:id/salaryListView")).findElementsById("gr.tessera.easytec:id/salaryCodeFirstlayout");

        do{
            if(salaries.get(index).findElementById("gr.tessera.easytec:id/firstTxt").getText().matches(salaryCode)){
                foundElement=!foundElement;
            }else{
                index++;
            }

        }while(!foundElement);





        MobileElement lastSalary = salaries.get(index);
        assert lastSalary.findElement(By.id("gr.tessera.easytec:id/salaryAmountTxt")).getText().contains(amount);
        assert lastSalary.findElement(By.id("gr.tessera.easytec:id/secondTxt")).getText().equals(employee);
        assert lastSalary.findElement(By.id("gr.tessera.easytec:id/firstTxt")).getText().equals(salaryCode);

        lastSalary.click();
        List<MobileElement> fields = driver.findElements(By.id("gr.tessera.easytec:id/valueTxt"));

        assert fields.get(0).getText().equals(employee);
        assert !Boolean.parseBoolean(fields.get(0).getAttribute("clickable"));

        assert fields.get(2).getText().startsWith(salaryCode);
        assert !Boolean.parseBoolean(fields.get(2).getAttribute("clickable"));

        assert fields.get(3).getText().contains(StartTime);
        assert !Boolean.parseBoolean(fields.get(3).getAttribute("clickable"));

        assert fields.get(4).getText().equals(FinishTime);
        assert !Boolean.parseBoolean(fields.get(4).getAttribute("clickable"));

        assert fields.get(5).getText().contains(amount);
        assert !Boolean.parseBoolean(fields.get(5).getAttribute("clickable"));

        assert fields.get(6).getText().equals(info);
        assert !Boolean.parseBoolean(fields.get(6).getAttribute("clickable"));

    }

    //**********************************************************************************************************************//



    @Then("^Salary with salary type (.*), info (.*), and days (.*) for employee (.*) is locked$")
    public void check_the_salary_u3_has_correct_values(String salaryCode, String info,String days, String employee){


        List<MobileElement> salaries = driver.findElement(By.id("gr.tessera.easytec:id/salaryListView")).findElementsById("gr.tessera.easytec:id/salaryCodeFirstlayout");

        do{
            if(salaries.get(index).findElementById("gr.tessera.easytec:id/firstTxt").getText().matches(salaryCode)){
                foundElement=!foundElement;
            }else{
                index++;
            }

        }while(!foundElement);

        MobileElement lastSalary = salaries.get(index);
        assert lastSalary.findElement(By.id("gr.tessera.easytec:id/salaryAmountTxt")).getText().contains(days);
        assert lastSalary.findElement(By.id("gr.tessera.easytec:id/secondTxt")).getText().equals(employee);
        assert lastSalary.findElement(By.id("gr.tessera.easytec:id/firstTxt")).getText().equals(salaryCode);

        lastSalary.click();
        List<MobileElement> fields = driver.findElements(By.id("gr.tessera.easytec:id/valueTxt"));

        assert fields.get(0).getText().equals(employee);
        assert !Boolean.parseBoolean(fields.get(0).getAttribute("clickable"));

        assert fields.get(2).getText().startsWith(salaryCode);
        assert !Boolean.parseBoolean(fields.get(2).getAttribute("clickable"));

        assert fields.get(3).getText().contains(days);
        assert !Boolean.parseBoolean(fields.get(3).getAttribute("clickable"));

        assert fields.get(4).getText().equals(info);
        assert !Boolean.parseBoolean(fields.get(4).getAttribute("clickable"));

    }



    //********************************************************************************************************/

    @When("^Synchronize$")
    public void synchronize() {
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME+":id/sync")).click();

        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                driver.findElement(By.id("android:id/button2")).click();
            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }
        }
    }



    @And("^Sync$")
    public void Sync(){
        driver.findElement(By.id("gr.tessera.easytec:id/sync")).click();
    }

/*
    @When("^Go to Hours and Costs$")
    public void go_to_Hours_and_Costs() {
        driver.findElement(By.xpath("//android.widget.TextView[@text='" + resources.getString("hours_and_costs") + "']")).click();

    }
*/
    @When("^Add an hour$")
    public void add_an_hour() {
        driver.findElement(By.id("gr.tessera.easytec:id/add_hours")).click();
    }

    @Then("^Select employee (.*)$")
    public void select_employee_for_salary(String employee) {
        MobileElement employeeSpinner=null;

        if(AndroidSetup.PACKAGE_NAME.matches("gr.tessera.easytec")){
             employeeSpinner=driver.findElement(By.id("gr.tessera.easytec:id/employee_spinner"));
        }else{
             employeeSpinner =  driver.findElementByClassName("android.widget.Spinner");
        }

        employeeSpinner.click();


        TouchAction action = new TouchAction(driver);



        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.textContains(\""+employee+"\"))").click();

    }

    @Then("^(.*) is not driving$")
    public void Check_participant_is_not_driving(String employee){

        MobileElement employeeSpinner= driver.findElementByClassName("android.widget.Spinner");
        employeeSpinner.click();
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.textContains(\""+employee+"\"))").click();
        assert     driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.textContains(\"Start Driving\"))").isDisplayed();
    }

    @Then("^(.*) is not resting$")
    public void Check_participant_is_not_resting(String employee){

        MobileElement employeeSpinner= driver.findElementByClassName("android.widget.Spinner");
        employeeSpinner.click();
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.textContains(\""+employee+"\"))").click();
        assert     driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.textContains(\"Start resting\"))").isDisplayed();
    }


    @When("^Set info (.*)$")
    public void set_info_for_salary(String info){
        driver.findElement(By.id("gr.tessera.easytec:id/dicText")).clear();
        driver.findElement(By.id("gr.tessera.easytec:id/dicText")).sendKeys(info);
        driver.hideKeyboard();

    }

    @And("^Check  employee (.*)$")
    public void  check_employee_text(String employee){
        assert driver.findElementByClassName("android.widget.CheckedTextView").getText().contains(employee);
    }

    @And("^Set employees (.*)$")
    public void set_empolyee(String employee){
        MobileElement employeeSpinner = driver.findElement(By.id("android:id/text1"));
        employeeSpinner.click();
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.textContains(\""+employee+"\"))").click();
    }

    @And("^Select participant employee (.*)$")
    public void select_participant_employee(String employee){
        driver.findElementByClassName("android.widget.CheckedTextView").click();
        driver.findElementByAndroidUIAutomator("new UiSelector().textContains(" + employee + ")").click();
    }

    @And("^Check Start Date  (.*)$")
    public void and_check_start_date_and_time(String StartDate){
        assert driver.findElementsById("gr.tessera.easytec:id/dateTimePickerBtn").get(0).getText().contains(StartDate);
    }

    @And("^Check Finish Date to (.*)$")
    public void check_finish_date_to(String FinishDate){
        assert driver.findElementsById("gr.tessera.easytec:id/dateTimePickerBtn").get(1).getText().contains(FinishDate);
    }

    @And("^Check info (.*)$")
    public void check_info_this_is_the_message(String Message){
        assert driver.findElementsById("gr.tessera.easytec:id/dicText").get(0).getText().contains(Message);
    }

    @And("^Check Executed text (.*)$")
    public void check_executed_text_this_is_executed_text(String executedText){
        System.out.println("The text "+executedText);
        System.out.println("The originial "+ driver.findElementsById("gr.tessera.easytec:id/dicText").get(1).getText());
        assert driver.findElementsById("gr.tessera.easytec:id/dicText").get(1).getText().contains(executedText);
    }

    @And("^Set executed text (.*)$")
    public void set_executed_text_this_is_the_executed_text(String executedText) throws InterruptedException {
        Thread.sleep(1500);
        driver.findElementsById("gr.tessera.easytec:id/dicText").get(1).clear();
        driver.findElementsById("gr.tessera.easytec:id/dicText").get(1).sendKeys(executedText);
        driver.hideKeyboard();

    }

    @When("^Select salary type (.*)$")
    public void select_salary_type(String salaryCode){
        MobileElement salarySpinner = driver.findElement(By.id("gr.tessera.easytec:id/salary_code_spinner"));
        salarySpinner.click();
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.textContains(\""+salaryCode+"\"))").click();

    }

    @When("^Select moisture type (.*)$")
    public void select_moisture_type(String salaryCode){
        MobileElement salarySpinner = driver.findElement(By.id("gr.tessera.easytec.herrmann:id/valueSp"));
        salarySpinner.click();
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.textContains(\""+salaryCode+"\"))").click();

    }


    @And("^Select place type (.*)$")
    public void select_place_type_testing(String option){
        driver.findElements(By.id("gr.tessera.easytec.herrmann:id/valueSp")).get(1).click();;
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.textContains(\""+option+"\"))").click();
    }

    @And("^Click on  measurement name (.*)$")
    public void click_on_measurement_name_testing(String measurement){
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.textContains(\""+measurement+"\"))").click();
    }

    @And("^Set date  (.*)$")
    public void set_date(String date) throws InterruptedException {
        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/datePickerBtn")).click();
        Set_Date(date);
        driver.findElement(By.id("android:id/button1")).click();
    }


    @And("^Set moisture percentanse (.*)$")
    public void set_moistur_percentanse(String amount){
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.resourceId(\"gr.tessera.easytec.herrmann:id/amountTxt\"))").click();
        addAmount(amount);
    }



    @And("^Set temperature (.*)$")
    public void set_temperature(String amount){
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.resourceId(\"gr.tessera.easytec.herrmann:id/amountTxt\"))").click();
        addAmount(amount);
    }

    @And("^Set air speed (.*)$")
    public void  set_air_speed(String amount){
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.resourceId(\"gr.tessera.easytec.herrmann:id/amountTxt\"))").click();
        addAmount(amount);
    }

    @And("^Set digits (.*)$")
    public void  set_digits(String amount){
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.resourceId(\"gr.tessera.easytec.herrmann:id/amountTxt\"))").click();
        addAmount(amount);
    }

    @And("^Set SKT (.*)$")
    public void  set_SKT(String amount){
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.resourceId(\"gr.tessera.easytec.herrmann:id/amountTxt\"))").click();
        addAmount(amount);
    }

    @And("^Click finish checkbox$")
    public void click_finish_checkbox(){
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.resourceId(\"gr.tessera.easytec.herrmann:id/valueChk\"))").click();
    }


    @When("^Set hours (.*)$")
    public void set_hours(String hours){
            driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.resourceId(\"gr.tessera.easytec:id/hours\"))").click();
        addAmount(hours);
    }

    @When("^Save the salary$")
    public void save_the_salary() {
        driver.findElement(By.id("gr.tessera.easytec:id/save")).click();
        assert driver.currentActivity().equals(".screens.SalaryActivity");
    }

    @Then("^Material with itemNo (.*) and quantity (.*) is locked$")
    public void material_with_itemNo_and_quantity_is_added(String itemNo, String quantity) {

        List<MobileElement> materials = driver.findElement(By.id("gr.tessera.easytec:id/orderMateriaListView")).findElementsByClassName("android.widget.LinearLayout");
        MobileElement lastMaterial = materials.get(materials.size() - 1);
        lastMaterial.click();
        List<MobileElement> fields = driver.findElements(By.id("gr.tessera.easytec:id/valueTxt"));

        assert fields.get(0).getText().equals(itemNo);
        assert !Boolean.parseBoolean(fields.get(0).getAttribute("clickable"));
        assert fields.get(8).getText().equals(quantity);
        assert !Boolean.parseBoolean(fields.get(8).getAttribute("clickable"));

    }

      @And("^Sort  by (.*) in (.*) order$")
    public void sort_material_by_srticle_no_in_mode(String Option , String SortingOption) throws InterruptedException {

        if(Option.contains("Document")||Option.matches("Date")||Option.contains("Type")||Option.contains("Picture")){
            driver.findElementById("gr.tessera.easytec:id/document_sort").click();

        }else{driver.findElementById("gr.tessera.easytec:id/sort").click();}


        if(SortingOption.matches("ascending")){
            driver.findElementByAndroidUIAutomator("new UiSelector().textContains(\""+Option+" (A-Z)\")").click();

        }else{
            driver.findElementByAndroidUIAutomator("new UiSelector().textContains(\""+Option+" (Z-A)\")").click();
        }
    }


    @Then("^Materials are sorted by (.*) in (.*) order$")
    public void materials_are_sorted_by_document_name_in(String Option , String SortingOption){
        List<MobileElement> mNameElements = driver.findElementsById("gr.tessera.easytec:id/documentNameTxt");
        List<MobileElement> mDateElements = driver.findElementsById("gr.tessera.easytec:id/documentDateTxt");

         if(SortingOption.matches("ascending")&&Option.contains("Document")||Option.contains("Picture")&&SortingOption.matches("ascending")){
              assert driver.findElementById("gr.tessera.easytec:id/headerSortTxt").getText().contains(Option+" (A-Z)");
             assert mNameElements.get(0).getText().compareToIgnoreCase(mNameElements.get(1).getText())<0;

         }else if(SortingOption.matches("descending")&&Option.contains("Document")||Option.contains("Picture")&&SortingOption.matches("descending")){
             assert driver.findElementById("gr.tessera.easytec:id/headerSortTxt").getText().contains(Option+" (Z-A)");
             assert mNameElements.get(0).getText().compareToIgnoreCase(mNameElements.get(1).getText())>0;

         }else if(SortingOption.matches("ascending")&&Option.contains("Type")){
             assert driver.findElementById("gr.tessera.easytec:id/headerSortTxt").getText().contains(Option+" (A-Z)");


         }else if(SortingOption.matches("descending")&&Option.contains("Type")){
             assert driver.findElementById("gr.tessera.easytec:id/headerSortTxt").getText().contains(Option+" (Z-A)");

         }else if(SortingOption.matches("ascending")&&Option.contains("Date")){
             assert driver.findElementById("gr.tessera.easytec:id/headerSortTxt").getText().contains(Option+" (A-Z)");
             assert mDateElements.get(0).getText().compareToIgnoreCase(mDateElements.get(1).getText())<0;

         }else if(SortingOption.matches("descending")&&Option.contains("Date")){
             assert driver.findElementById("gr.tessera.easytec:id/headerSortTxt").getText().contains(Option+" (Z-A)");
             assert mDateElements.get(0).getText().compareToIgnoreCase(mDateElements.get(1).getText())>0;
         }



    }




    @Then("^Check order sorted by (.*) in (.*) order$")
    public void check_order_sorted_by_start_date_in_ascending_order(String option,String mode){
            List<MobileElement> StartDates = driver.findElementsById("gr.tessera.easytec:id/orderStartDateTxt");
            List<MobileElement> FinishDates = driver.findElementsById("gr.tessera.easytec:id/orderFinishDateTxt");
            List<MobileElement> Adreess = driver.findElementsById("gr.tessera.easytec:id/orderAddressTxt");
            List<MobileElement> Orders = driver.findElementsById("gr.tessera.easytec:id/orderIdTxt");
            List<MobileElement> Names = driver.findElementsById("gr.tessera.easytec:id/customNameTxt");





        if(option.contains("Date")&&mode.matches("ascending")){
                assert driver.findElementById("gr.tessera.easytec:id/headerSortTxt").getText().contains(option);
                assert StartDates.get(0).getText().compareToIgnoreCase(StartDates.get(1).getText())<0;

        }else if(option.contains("Date")&&mode.matches("descending")){
                assert driver.findElementById("gr.tessera.easytec:id/headerSortTxt").getText().contains(option);
                assert FinishDates.get(0).getText().compareToIgnoreCase(FinishDates.get(1).getText())>0;

        }else if(option.contains("Address")&&mode.matches("ascending")){
                assert driver.findElementById("gr.tessera.easytec:id/headerSortTxt").getText().contains(option);
                assert Adreess.get(0).getText().compareToIgnoreCase(Adreess.get(1).getText())<0;

        }else if(option.contains("Address")&&mode.matches("descending")){
                assert driver.findElementById("gr.tessera.easytec:id/headerSortTxt").getText().contains(option);
                assert Adreess.get(0).getText().compareToIgnoreCase(Adreess.get(1).getText())>0;

        }else if(option.contains("Name")&&mode.matches("ascending")){
                assert driver.findElementById("gr.tessera.easytec:id/headerSortTxt").getText().contains(option);
                assert Names.get(0).getText().compareToIgnoreCase(Names.get(1).getText())<0;

        }else if(option.contains("Name")&&mode.matches("descending")){
                assert driver.findElementById("gr.tessera.easytec:id/headerSortTxt").getText().contains(option);
                assert Names.get(0).getText().compareToIgnoreCase(Names.get(1).getText())>0;

        }else if(option.contains("Order")&&mode.matches("ascending")){
                assert driver.findElementById("gr.tessera.easytec:id/headerSortTxt").getText().contains(option);
                assert Orders.get(0).getText().compareToIgnoreCase(Orders.get(1).getText())<0;

        }else if(option.contains("Order")&&mode.matches("descending")){
                assert driver.findElementById("gr.tessera.easytec:id/headerSortTxt").getText().contains(option);
                assert Orders.get(0).getText().compareToIgnoreCase(Orders.get(1).getText())>0;
        }


    }

    //************************************************//
    @And("^Set Start Date (.*) and Time (.*)$")
    public void setStartTime(String Date,String Time) throws InterruptedException {

        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.resourceId(\"gr.tessera.easytec:id/start_time\"))").click();
        SetDate(Date);
        SetTime(Time);
        driver.findElement(By.id("android:id/button1")).click();

    }


    @And("^Set Date value (.*)$")
    public void set_date_value(String Date) throws InterruptedException {
        driver.findElementById("gr.tessera.easytec:id/datePickerBtn").click();
        SetDate(Date);
        driver.findElementById("android:id/button1").click();

    }



    //*****************************************************/

    @And("^Set Start date (.*)$")
    public void set_start_date(String Date) throws InterruptedException {

        driver.findElement(By.id("gr.tessera.easytec:id/start_date")).click();
        Set_Date(Date);
        driver.findElement(By.id("android:id/button1")).click();
    }


//***********************************************************/

    @And("^Set Finish date (.*)$")
    public void set_the_finish_date(String Date) throws InterruptedException {

        driver.findElement(By.id("gr.tessera.easytec:id/finish_date")).click();
        Set_Date(Date);
        driver.findElement(By.id("android:id/button1")).click();
    }


    @And("^Add Measurement point$")
    public void add_measurement_point(){
        System.out.println("The Cureeent Activity "+driver.currentActivity());
            driver.findElementById("gr.tessera.easytec.herrmann:id/add").click();
    }

    @And("^Set Measurement name (.*)$")
    public void set_measurement_name_testing_measurement(String name){
        driver.findElementById("gr.tessera.easytec.herrmann:id/valueTxt").setValue(name);
    }

    @Then("^Check if finish flag exist$")
    public void check_if_finish_flag_exist(){
    }


    @And("^Bullet is enable and visible$")
    public void bollet_is_enable_and_visible(){
            assert driver.findElementsById("gr.tessera.easytec.ks:id/radioButton").get(0).isEnabled();
    }

    @Then("^Email the report for email (.*)$")
    public void emai_the_report(String email){
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.resourceId(\"gr.tessera.easytec:id/email\"))").click();
        //set email
        driver.findElementByClassName("android.widget.EditText").setValue(email);
        driver.findElementByXPath("//android.widget.Button[@text='OK']").click();
        driver.findElementByXPath("//android.widget.Button[@text='OK']").click();
    }




    @And("^The (.*) is (.*)$")
    public void employee_is_main_participant(String employeeName,String role){
        //click on the employees arrow
        if(AndroidSetup.PACKAGE_NAME.matches("gr.tessera.easytec")){
        driver.findElementById("gr.tessera.easytec:id/arrowRight").click();
            //check if employee name exist and it contains text main participant
            assert driver.findElementByXPath("//android.widget.TextView[contains(@text,'"+employeeName+"')]").getText().contains(role);
        }else{
         assert driver.findElementByXPath("//android.widget.TextView[contains(@text,'"+employeeName+"')]").isDisplayed() && driver.findElementByXPath("//android.widget.TextView[contains(@text,'"+role+"')]").isDisplayed();
        }


    }



    @And("^Version is (.*), Registration is (.*), Address is (.*), Website is (.*), Tel is (.*), Email is (.*), Owner is (.*)$")
    public void check_app_informations(String version,String registration,String address,String website,String tel,String email,String owner){

        List<MobileElement> VersionData = driver.findElementsById("android:id/text2");

        //check for the version
            assert VersionData.get(0).getText().contains(version);
        //chreck fot the registration
            assert VersionData.get(1).getText().contains(registration);
        //check for the Address
            assert VersionData.get(2).getText().contains(address);
        //check for the website
            assert VersionData.get(3).getText().matches(website);
        //check for the tel
            assert VersionData.get(4).getText().contains(tel);
        //check for the email
            assert VersionData.get(6).getText().matches(email);
        //check for the owner
            assert VersionData.get(7).getText().matches(owner);

    }






    @And("^Set Finish Date to (.*) and Time (.*)$")
    public void  and_set_finish_time_to_27(String Date,String Time) throws InterruptedException {
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.resourceId(\"gr.tessera.easytec:id/finish_time\"))").click();
        SetDate(Date);
        SetTime(Time);
        driver.findElement(By.id("android:id/button1")).click();

    }


    @And("Set participant Start Date (.*) and Time (.*)$")
    public void  and_set_participant_start_time_to(String Date,String Time) throws InterruptedException {
        driver.findElementsById("gr.tessera.easytec:id/dateTimePickerBtn").get(0).click();
        SetDate(Date);
        SetTime(Time);
        driver.findElement(By.id("android:id/button1")).click();

    }



    @And("Set participant Finish Date to (.*) and Time (.*)$")
    public void  and_set_participant_finish_time_to(String Date,String Time) throws InterruptedException {
        driver.findElementsById("gr.tessera.easytec:id/dateTimePickerBtn").get(1).click();
        SetDate(Date);
        SetTime(Time);
        driver.findElement(By.id("android:id/button1")).click();

    }


    @And("^Set days (.*)$")
    public void seeTheDays(String days){

        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.resourceId(\"gr.tessera.easytec:id/days\"))").click();
        addAmount(days);
    }



    @And("^Press  save button$")
    public void click_the_save_button(){
        driver.findElement(By.id("gr.tessera.easytec:id/save")).click();
    }


    @Then("^Salary is invalid$")
    public void check_the_salary_is_invalid(){

            assert driver.findElementById("gr.tessera.easytec:id/warning")!=null;
            assert driver.currentActivity().contains("MultipleHoursFormActivity");
    }





    @And("^Pressing (.*)$")
    public void pressing_start_driving(String driving_option){
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.textContains(\""+driving_option+"\"))").click();
    }

    @Then("^The (.*) text (.*)$")
    public void the_optional_text_exist(String optional_text,String situation){

        try{
           assert    driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoVie|.textContains(\""+optional_text+"\"))").isDisplayed();
        }catch(Exception e){
            assert situation.matches("not exist");
        }

    }


    @And("^Long tap on order (.*)$")
    public void long_tap_on_order(String element){
        longClickOnaElement(driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.textContains(\""+element+"\"))"));

    }


    @And("^Tap the delete button$")
    public void tap_the_delete_button(){
        driver.findElementById("gr.tessera.easytec:id/delete").click();

    }


    @And("^Tap on the OK button$")
    public void tap_on_the_ok_button(){
            driver.findElementById("android:id/button1").click();
    }


    @And("^Order (.*) is not in the order list$")
    public void order_is_not_in_the_order_list(String order){

        assert  driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.textContains(\""+order+"\"))")==null;
    }

    @And("^I do not see menu item (.*)$")
    public void i_do_not_see_menu_item(String menuOption){
            checkMenuOptionNotExistion(menuOption);
    }



    @And("^I see menu item (.*)$")
    public void i_see_menu_item(String menuOption){
        checkMenuOptionExistion(menuOption);
    }



    @And("^Click the (.*)$")
    public void click_the_checklist_option(String checkListOption){
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.textContains(\""+checkListOption+"\"))").click();
    }


    @And("^Set the value (.*)$")
    public void set_the_value(String value){
        driver.findElementById("gr.tessera.easytec:id/amountTxt").click();
        addAmount(value);
    }

    @And("^Tick the value$")
    public void tick_the_value(){
            driver.findElementById("gr.tessera.easytec:id/valueChk").click();
    }

    @And("^Save$")
    public void save(){
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME+":id/save")).click();
    }

    @And("^Click on the (.*)$")
    public void click_on_the_proceed_button(String ButtonText){
       // driver.findElementByXPath("//android.widget.Button[contains(@text,'"+ButtonText+"')]").click();
        driver.findElementById("android:id/button1").click();
    }

    @And("^Click  the signatures$")
    public void click_on_the_signatures(){
        driver.navigate().back();
        driver.findElementById("gr.tessera.easytec:id/signCheckList").click();
    }



    @Then("^Check if the value is space$")
    public void check_if_the_value_is_space(){
        assert driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.resourceId(\"gr.tessera.easytec:id/hours\"))").getText().matches("");
    }

    @And("^Confirm the finalize of the order$")
    public void confirm_the_finalize_of_the_order(){
        driver.findElementById("android:id/button1").click();
    }

    @And("^Set the Signatures$")
    public void set_the_signatures() throws InterruptedException {
        Thread.sleep(1000);
        //***
        //   Find and click the first tab Process
        //***

        //get touchAction object
        TouchAction touchAction = new TouchAction(driver);

        //find all the tab objects
        List<MobileElement> Tab_menu = driver.findElement(By.className("android.widget.TabWidget")).findElements(By.className("android.widget.LinearLayout"));

        //find the Signature Field
        MobileElement SignatureImageView = driver.findElement(By.id("gr.tessera.easytec:id/pcSignatureArea"));

        // find the coordinates of the Signature view
        int x = SignatureImageView.getLocation().getX();
        int y = SignatureImageView.getLocation().getY();

        //set the touch Motion event
        touchAction.longPress(SignatureImageView).moveTo(x+200, y).release();
        Thread.sleep(1000);
        touchAction.perform();



        //****
        //   Click on the second tab
        //****
        // get the tab object
        Tab_menu.get(1).click();

        //find the Signature Field
        MobileElement  SignatureImageView1= driver.findElement(By.id("gr.tessera.easytec:id/poSignatureArea"));
        // find the coordinates of the Signature view
        int x1 = SignatureImageView1.getLocation().getX();
        int y1 = SignatureImageView1.getLocation().getY();

        //get touchAction object
        TouchAction touchAction1 =  new TouchAction(driver);

        //set the touch Motion event
        touchAction1.longPress(SignatureImageView1).moveTo(x1+200, y1).release();
        Thread.sleep(1000);
        touchAction1.perform();

        //****
        //   Click on the third tab
        //****

        // get the tab object
        Tab_menu.get(2).click();

        Thread.sleep(1500);

        //find the Signature Field
        MobileElement SignatureImageView2= driver.findElement(By.id("gr.tessera.easytec:id/ccSignatureArea"));

        // find the coordinates of the Signature view
        int  x2 = SignatureImageView2.getLocation().getX();
        int  y2 = SignatureImageView2.getLocation().getY();

        //get touchAction object
        TouchAction touchAction2 =  new TouchAction((MobileDriver)driver);


        //set the touch Motion event
        touchAction2.longPress(SignatureImageView2).moveTo(x2+200, y2).release();
        touchAction2.perform();

        // set a signature name (*** needed ***)
        driver.findElement(By.id("gr.tessera.easytec:id/ccNameSignEditText")).clear();
        driver.findElement(By.id("gr.tessera.easytec:id/ccNameSignEditText")).setValue("test");
        driver.hideKeyboard();
        //****
        //   Click on the fourth tab
        //****
        // get the tab object
        Tab_menu.get(3).click();
        //find the Signature Field
        MobileElement   SignatureImageView3= driver.findElement(By.id("gr.tessera.easytec:id/coSignatureArea"));

        // find the coordinates of the Signature view
        int x3 = SignatureImageView3.getLocation().getX();
        int y3 = SignatureImageView3.getLocation().getY();

        //get touchAction object
        TouchAction touchAction3 =  new TouchAction((MobileDriver)driver);

        //set the touch Motion event
        touchAction3.longPress(SignatureImageView3).moveTo(x3+200, y3).release();
        touchAction3.perform();


        // set a signature name (*** needed ***)
        driver.findElement(By.id("gr.tessera.easytec:id/coNameSignEditText")).sendKeys("test");
        driver.hideKeyboard();

        // save the whole process
    }


    @Then("^Check Drag n Drop event$")
    public void check_drag_n_drop_event() throws InterruptedException {
        MobileElement RecyclerView  = driver.findElement(By.className("android.support.v7.widget.RecyclerView"));
        List<MobileElement> RelativeLayoutsList  = RecyclerView.findElements(By.className("android.widget.RelativeLayout"));


        if(driver.findElements(By.xpath("//android.widget.TextView[@text='Employees']")).size()!=0) {
             EMPLOYMENT_EL = driver.findElement(By.xpath("//android.widget.TextView[@text='Employees']"));
             HOURS_AND_COSTS_EL = driver.findElement(By.xpath("//android.widget.TextView[@text='Hours and Costs']"));
             CHECKLIST_EL = driver.findElement(By.xpath("//android.widget.TextView[@text='Checklists/Signatures']"));
        }else{
            throw new NullPointerException("The is none Employees  Object in Ui panel");
        }

        TouchAction touchAction = new TouchAction((MobileDriver)driver).longPress(EMPLOYMENT_EL).moveTo(HOURS_AND_COSTS_EL).release();
        touchAction.perform();


        Thread.sleep(2000);


        while(FOUND_ELEMENT_FLAG==false){


            if(RelativeLayoutsList.get(COUNTER).findElement(By.className("android.widget.TextView")).getText().matches("Employees")){

                System.out.print("First" + String.valueOf(COUNTER)+"\n");
                CURRENT_POSITION = COUNTER;
                FOUND_ELEMENT_FLAG = true;

            }else{
                COUNTER++;
            }
        }

        Thread.sleep(2000);
        EMPLOYMENT_EL = driver.findElement(By.xpath("//android.widget.TextView[@text='Employees']"));

        TouchAction touchActionBack = new TouchAction((MobileDriver)driver).longPress(EMPLOYMENT_EL).moveTo(CHECKLIST_EL).release();
        touchActionBack.perform();


        Thread.sleep(2000);
        FOUND_ELEMENT_FLAG = false ;
        COUNTER=0;

        while(FOUND_ELEMENT_FLAG==false){
            System.out.print("SECOND\n");
            if(RelativeLayoutsList.get(COUNTER).findElement(By.className("android.widget.TextView")).getText().matches("Employees")){

                LASTPOSITION = COUNTER;
                FOUND_ELEMENT_FLAG = true;

            }else{
                COUNTER++;
            }
        }

        assert PAST_POSITION==LASTPOSITION;
    }




    @And("^Give employee signature$")
    public void give_employee_signature() throws InterruptedException {


        driver.findElementById("gr.tessera.easytec:id/finish").click();

        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {
            driver.findElementById("android:id/button1").click();
        }

        driver.findElement(By.id("android:id/button1")).click();

        MobileElement SignatureImageView = driver.findElement(By.id("gr.tessera.easytec:id/signatureArea"));
        TouchAction touchAction = new TouchAction(driver);
        // find the coordinates of the Signature view
        int x = SignatureImageView.getLocation().getX();
        int y = SignatureImageView.getLocation().getY();
        //set the touch Motion event
        touchAction.longPress(SignatureImageView).moveTo(x+200, y).release();
        Thread.sleep(1000);
        touchAction.perform();




    }

    @And("^Give participant employee signature$")
    public void give_participant_employee_signature() throws InterruptedException {


        driver.findElementById("gr.tessera.easytec:id/finish").click();
        MobileElement SignatureImageView = driver.findElement(By.id("gr.tessera.easytec:id/signatureArea"));
        TouchAction touchAction = new TouchAction(driver);
        // find the coordinates of the Signature view
        int x = SignatureImageView.getLocation().getX();
        int y = SignatureImageView.getLocation().getY();
        //set the touch Motion event
        touchAction.longPress(SignatureImageView).moveTo(x+200, y).release();
        Thread.sleep(1000);
        touchAction.perform();




    }

    @And("^Go back$")
    public void go_back(){
        driver.navigate().back();
        driver.navigate().back();
    }

    @And("^Click finalize flag$")
    public void click_finalize_flag(){
        driver.findElementById("gr.tessera.easytec:id/finishOrder").click();

    }

    @And("^Set order state (.*)$")
    public void set_order_state_value(String Value){

        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.textContains(\"Select order state\"))").click();
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.textContains(\""+Value+"\"))").click();

    }

    @Then("^Check if in the order (.*) flag exist$")
    public void check_if_in_the_order_flag_exist(String orderNo){

    MobileElement order=driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.textContains(\""+orderNo+"\"))");

        try{
            assert driver.findElement(By.id("R.drawable.ic_finish"))!=null;

        }catch (Exception e){
            assert true;
        }

    }


    @Then("^Email icon is not visible$")
    public void the_email_icon_is_not_visible(){
        try{
            assert   !driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.resourceId(\"gr.tessera.easytec:id/email\"))").isDisplayed();
        }catch(Exception e){
            assert true;
        }
    }


    @Then("^Check if the document (.*) in order (.*)$")
    public void check_if_the_document_exist_in_order(String option,String orderNo){
            //check for the for the order with document icon
            List<MobileElement> mLinearList = driver.findElementByClassName("android.widget.ListView").findElementsByClassName("android.widget.LinearLayout");
            int index=2;
                if(mLinearList.get(index).findElementById("gr.tessera.easytec:id/orderIdTxt").getText().matches(orderNo)){
                        if(option.matches("exist")){
                            assert driver.findElementById("gr.tessera.easytec:id/documentIcon").isDisplayed();
                        }else{
                            assert !driver.findElementById("gr.tessera.easytec:id/documentIcon").isDisplayed();
                        }
                    index+=2;
                }else{index+=2;}
    }

    @And("^And new item (.*)$")
    public void and_new_item(String option){
        driver.findElementById("gr.tessera.easytec:id/add_"+option).click();
    }



    private void goToMainMenu() {
        while (!driver.currentActivity().equals(MAIN_ACTIVITY)) {
            driver.navigate().back();
        }
    }

    private void goToMenuOption(String Menuoption){
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.textContains(\""+Menuoption+"\"))").click();
    }

    private void selectMaterial(String itemNo) {

        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.textContains(\""+itemNo+"\"))").click();
    }

    private void goToOrder(String orderNo) {
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.textContains(\""+orderNo+"\"))").click();
    }

    private void addAmount(String amount) {
        driver.findElement(By.id("gr.tessera.easytec:id/amountText")).clear();
        driver.findElement(By.id("gr.tessera.easytec:id/amountText")).sendKeys(amount);
        driver.findElement(By.xpath("//android.widget.Button[@text='OK']")).click();
    }

    private void SetDate(String Date) throws InterruptedException {


        String[] SpareDate = Date.split("/");


        List<MobileElement> numberpickers  = driver.findElementsByClassName("android.widget.NumberPicker");
        MobileElement dayPicker =numberpickers.get(0).findElementById("android:id/numberpicker_input");

        boolean found = false;

        do{
            if(Integer.valueOf(dayPicker.getText())>Integer.valueOf(SpareDate[0])) {

                driver.findElement(By.xpath("//android.widget.Button[contains(@text,'"+(Integer.valueOf(Integer.valueOf(dayPicker.getText()))-1)+"')]")).tap(1,2);
                Thread.sleep(1000); ;


            }else if(Integer.valueOf(dayPicker.getText())<Integer.valueOf(SpareDate[0])) {

                driver.findElement(By.xpath("//android.widget.Button[contains(@text,'"+(Integer.valueOf(Integer.valueOf(dayPicker.getText()))+1)+"')]")).tap(1,2);
                Thread.sleep(1000);

            }else if(Integer.valueOf(dayPicker.getText())==Integer.valueOf(SpareDate[0])){
                found=true;
            }


        }while(!found);

    }



    private void Set_Date(String Date) throws InterruptedException {


        String[] SpareDate = Date.split("/");
        List<MobileElement> numberpickers  = driver.findElementsByClassName("android.widget.NumberPicker");
        MobileElement dayPicker =numberpickers.get(0).findElementById("android:id/numberpicker_input");
        boolean found = false;

        do{
            if(Integer.valueOf(dayPicker.getText())>Integer.valueOf(SpareDate[0])) {

                driver.findElement(By.xpath("//android.widget.Button[contains(@text,'"+(Integer.valueOf(Integer.valueOf(dayPicker.getText()))-1)+"')]")).tap(1,2);
                Thread.sleep(2000);


            }else if(Integer.valueOf(dayPicker.getText())<Integer.valueOf(SpareDate[0])) {

                driver.findElement(By.xpath("//android.widget.Button[contains(@text,'"+(Integer.valueOf(Integer.valueOf(dayPicker.getText()))+1)+"')]")).tap(1,2);
                Thread.sleep(2000);

            }else if(Integer.valueOf(dayPicker.getText())==Integer.valueOf(SpareDate[0])){
                found=true;
            }


        }while(!found);

    }


    public void SetTime(String Time) throws InterruptedException {

        String[] SpareTime = Time.split(":");

        List<MobileElement> numberpickers  = driver.findElementByClassName("android.widget.TimePicker").findElementsByClassName("android.widget.NumberPicker");
        MobileElement hourPicker =numberpickers.get(0).findElementById("android:id/numberpicker_input");
        MobileElement minutePicker =numberpickers.get(1).findElementById("android:id/numberpicker_input");

        boolean found = false;


        do{

            System.out.println("From the text" + String.valueOf(hourPicker.getText()));
            System.out.println("From the inside" + String.valueOf(SpareTime[0]));
            System.out.println("From the inside" + numberpickers.get(0).findElementByClassName("android.widget.Button").getText());

            if(Integer.valueOf(hourPicker.getText())>Integer.valueOf(SpareTime[0])){

                numberpickers.get(0).findElementsByClassName("android.widget.Button").get(0).tap(1,2);
                Thread.sleep(1000);


            }else if(Integer.valueOf(hourPicker.getText())<Integer.valueOf(SpareTime[0])){
                numberpickers.get(0).findElementsByClassName("android.widget.Button").get(1).tap(1,2);
                Thread.sleep(1000);


            }else if(Integer.valueOf(hourPicker.getText())==Integer.valueOf(SpareTime[0])) {
                found = true;
            }

            Thread.sleep(2000);

        }while(!found);



        found=false;

        do{



            if(Integer.valueOf(minutePicker.getText())>Integer.valueOf(SpareTime[1])){

                numberpickers.get(1).findElementsByClassName("android.widget.Button").get(0).tap(1,2);
                Thread.sleep(2000);


            }else if(Integer.valueOf(minutePicker.getText())<Integer.valueOf(SpareTime[1])){
                numberpickers.get(1).findElementsByClassName("android.widget.Button").get(1).tap(1,2);
                Thread.sleep(2000);


            }else if(Integer.valueOf(minutePicker.getText())==Integer.valueOf(SpareTime[1])) {
                found = true;
            }

            Thread.sleep(2000);

        }while(!found);



    }


    public static long fromDateToTimestamp(String dateTime, String format) throws ParseException {

        SimpleDateFormat dateFormatter = getSimpleDateFormat(format);

        Calendar dateDay = Calendar.getInstance();
        dateDay.setTime(dateFormatter.parse(dateTime));

        long amount = (dateDay.getTimeInMillis() / 1000);

        return amount;
    }

    private static SimpleDateFormat getSimpleDateFormat(String format) {
        String finalFormat = format;

        if (format.equalsIgnoreCase(APP_DATETIME)) {

                finalFormat = APP_DATETIME_EN;

        } else if (format.equalsIgnoreCase(APP_DATE)) {
                finalFormat = APP_DATE_EN;

        }
        return (new SimpleDateFormat(finalFormat));
    }



    public static String formatDate(String date, String fromFormat, String toFormat) {

        SimpleDateFormat fromFormatter = getSimpleDateFormat(fromFormat);
        SimpleDateFormat toFormatter = getSimpleDateFormat(toFormat);

        String result;

        try {
            Date parsed = fromFormatter.parse(date);
            result = toFormatter.format(parsed);
        } catch (Exception e) {
            e.printStackTrace();
            result = date;
        }

        return result;
    }



    private void goToOrders(){
        driver.findElement(By.xpath("//android.widget.TextView[@text='Orders']")).click();
    }

    private void longClickOnaElement(MobileElement element){

        TouchAction touchAction = new TouchAction(driver);
        touchAction.longPress(element).release().perform();


    }





    private void checkMenuOptionExistion(String MenuOption){

        assert  driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.textContains(\""+MenuOption+"\"))")!=null;
    }

    private void checkMenuOptionNotExistion(String MenuOption){

        try{
        assert  !driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(.textContains(\""+MenuOption+"\"))").isDisplayed();
        }catch(Exception e){
            assert  true;
        }
    }


    public void login() {
        goToMainMenu();
        goToMenuOption("Settings");
        goToMenuOption("Account");
        // set SyncServer IP
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "serverAddressEditTxt")).sendKeys("192.168.0.133");

        if (!TextUtils.isEmpty("4011")) {
            driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "serverPortEditTxt")).sendKeys("4011");
        }

        // enable or disable SSL
        if (Boolean.parseBoolean("false")) {
            driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "sslChk")).click();
        }

        // save synchronization settings
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "save")).click();
        driver.findElementByXPath("//android.widget.Button[contains(@text,'OK')]").click();
        driver.findElementByXPath("//android.widget.Button[contains(@text,'Clear account data')]").click();
        driver.findElementByXPath("//android.widget.Button[contains(@text,'OK')]").click();


        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "sync")).click();

        // set username
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "usernameEditTxt")).sendKeys("fkrumscheid");
        // set password
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "passwordEditTxt")).sendKeys("123456");
        // synchronize (click OK)

        driver.findElement(By.id("android:id/button1")).click();


        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                driver.findElement(By.id("android:id/button2")).click();
            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }
        }

    }



}
