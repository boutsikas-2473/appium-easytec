package generic;

import TEST_CONFIGURATION_FILES.AndroidSetup;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.MobileElement;
import io.appium.java_client.SwipeElementDirection;
import io.appium.java_client.android.AndroidDriver;
import org.apache.http.util.TextUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by tesse on 4/19/2017.
 */
public class save_employee_data {


    private static AndroidDriver driver;
    private String START_DATE=null,FINISH_DATE=null;
    private int ListViewCounter = 0 ;


    //******************* Setting the driver and the capabilities
    @Before
    public void before() {
        try {
            AndroidSetup.deviceCapabilities();
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver = AndroidSetup.getDriver();


    }



    @Given("^I login  in to the  applications SyncServer with (.*),(.*),(.*)$")
    public void I_login_in_to_the_applications_SyncServer_with(String ip, String port, String ssl) {


        driver.findElement(By.xpath("//android.widget.TextView[@text='Settings']")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Account']")).click();

        // set SyncServer IP
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "serverAddressEditTxt")).sendKeys(ip);

        // set SyncServer port
        if (!TextUtils.isEmpty(port)) {
            driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "serverPortEditTxt")).sendKeys(port);
        }

        // enable or disable SSL
        if (Boolean.parseBoolean(ssl)) {
            driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "sslChk")).click();
        }

        // save synchronization settings
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "save")).click();

        driver.findElement(By.id("android:id/button1")).click();


        driver.findElement(By.id("gr.tessera.easytec:id/clearCredentialsBtn")).click();


        driver.findElement(By.id("android:id/button1")).click();

        // we are the main menu
    }



    @When("^I am signin  in the application  as a valid user in the app with (.*) and (.*)$")
    public void I_am_signin_in_the_application_as_a_valid_user_in_the_app_with(String username, String password) throws Throwable {

        // click synchronization action bar button
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "sync")).click();

        // set username
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "usernameEditTxt")).sendKeys(username);
        // set password
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "passwordEditTxt")).sendKeys(password);
        // synchronize (click OK)

        driver.findElement(By.id("android:id/button1")).click();


        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                driver.findElement(By.id("android:id/button2")).click();
            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }

        }

    }



    @And("^I will click on the orders  options button$")
    public void I_will_click_on_the_orders_options_button() throws Throwable {
        // //*[contains(@text, 'Sign Out')]

        driver.findElement(By.xpath("//android.widget.TextView[@text='Orders']")).click();
    }



    @And("^I will click on the user's given order (.*)$")
    public void I_will_click_on_the_user_s_given_order(String orderNo) throws Throwable {

        assert driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")) != null;
        if(driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")) != null){
            driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")).click();
        }else{

        }
    }


    @And("^I will find in the menu option employees and click it$")
    public void I_will_find_in_the_menu_option_employees_and_click_it() throws Throwable {


        if(driver.findElements(By.xpath("//android.widget.TextView[@text='Employees']")).size()!=0) {
            driver.findElement(By.xpath("//android.widget.TextView[@text='Employees']")).click();
        }else{
            throw new NullPointerException("The is none Employees  Object in Ui panel");
        }


    }


    @And("^I will take the value from user (.*) and save it$")
    public void I_will_take_the_value_from_user_and_save_it(String message) throws InterruptedException {

        //find the listview first

        WebElement listView = driver.findElement(By.className("android.widget.ListView"));
        List<WebElement> LinearLayoutList   = listView.findElements(By.className("android.widget.LinearLayout"));



        // find and click on the start date

        LinearLayoutList.get(3).findElement(By.className("android.widget.Button")).click();

        //click ok for the start date


        driver.findElement(By.id("android:id/button1")).click();

        // find and click the finish date
        LinearLayoutList.get(4).findElement(By.className("android.widget.Button")).click();

        // not set the finish date , value between 2-3 days difference

        Thread.sleep(1000);



        WebElement DatePicker =driver.findElement(By.id("gr.tessera.easytec:id/datePicker"));

        WebElement NumberPickerFirstLayout = DatePicker.findElement(By.className("android.widget.LinearLayout"));
        WebElement NumberPickerFirstLayout1 = NumberPickerFirstLayout.findElement(By.className("android.widget.LinearLayout"));
        WebElement NumberPickers = NumberPickerFirstLayout1.findElement(By.className("android.widget.NumberPicker"));
        MobileElement mobileElement = (MobileElement) NumberPickers;
        mobileElement.swipe(SwipeElementDirection.DOWN, 100);



        Thread.sleep(1000);


        // just click on the ok , in order to get the current day and time
        driver.findElement(By.id("android:id/button1")).click();



        //set the user message in the edittext

        Thread.sleep(1000);
        LinearLayoutList.get(5).findElement(By.className("android.widget.EditText")).sendKeys(message);

        driver.hideKeyboard();


        // collect allthe date for the next checkout

            START_DATE = LinearLayoutList.get(3).findElement(By.className("android.widget.Button")).getText();
            FINISH_DATE = LinearLayoutList.get(4).findElement(By.className("android.widget.Button")).getText();


        driver.findElement(By.id("gr.tessera.easytec:id/save")).click();
        Thread.sleep(1000);

        //find the back button

                List<WebElement> RelativeList = driver.findElements(By.className("android.widget.FrameLayout"));
                RelativeList.get(0).findElements(By.className("android.widget.ImageView")).get(1).click();





    }


    @Then("^I will check if the previous employee has been saved (.*)$")
    public void I_will_check_if_the_previous_employee_has_been_saved(String message){


        if(driver.findElements(By.xpath("//android.widget.TextView[@text='Employees']")).size()!=0) {
            driver.findElement(By.xpath("//android.widget.TextView[@text='Employees']")).click();
        }else{
            throw new NullPointerException("The is none Employees  Object in Ui panel");
        }

        WebElement listView = driver.findElement(By.className("android.widget.ListView"));
        List<WebElement> LinearLayoutList   = listView.findElements(By.className("android.widget.LinearLayout"));


        Assert.assertEquals("Is not the same",START_DATE,LinearLayoutList.get(3).findElement(By.className("android.widget.Button")).getText());
        Assert.assertEquals("Is not the same",FINISH_DATE,LinearLayoutList.get(4).findElement(By.className("android.widget.Button")).getText());
        Assert.assertEquals("Is not the same",message,LinearLayoutList.get(5).findElement(By.className("android.widget.EditText")).getText());


    }



    @AfterClass
    public static void teardown() throws Exception {
        //driver.quit();
        AndroidSetup.tearDown();
        System.out.println("Ran the after");
    }











}
