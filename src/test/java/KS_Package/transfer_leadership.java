package KS_Package;

import TEST_CONFIGURATION_FILES.AndroidSetup;
import TEST_CONFIGURATION_FILES.AndroidSetup_KS;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.android.AndroidDriver;
import org.apache.http.util.TextUtils;
import org.junit.AfterClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.List;

/**
 * Created by tesse on 4/25/2017.
 */
public class transfer_leadership {

    private static AndroidDriver driver;



    //******************* Setting the driver and the capabilities
    @Before
    public void before() {
        try {
            AndroidSetup_KS.deviceCapabilities();
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver = AndroidSetup_KS.getDriver();


    }



    @Given("^I have to sign in to the applications SyncServer with  (.*),(.*),(.*)$")
    public void I_have_to_sign_in_to_the_applications_SyncServer_with(String ip, String port, String ssl) {


        driver.findElement(By.xpath("//android.widget.TextView[@text='Settings']")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Account']")).click();

        // set SyncServer IP
        driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "serverAddressEditTxt")).sendKeys(ip);

        // set SyncServer port
        if (!TextUtils.isEmpty(port)) {
            driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "serverPortEditTxt")).sendKeys(port);
        }

        // enable or disable SSL
        if (Boolean.parseBoolean(ssl)) {
            driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "sslChk")).click();
        }

        // save synchronization settings
        driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "save")).click();

        driver.findElement(By.id("android:id/button1")).click();


        driver.findElement(By.id("gr.tessera.easytec.ks:id/clearCredentialsBtn")).click();


        driver.findElement(By.id("android:id/button1")).click();

        // we are the main menu
    }



    @When("^User has successfully sign in with credentials  (.*),(.*)$")
    public void User_has_successfully_sign_in_with_credentials(String username, String password) throws Throwable {

        // click synchronization action bar button
        driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "sync")).click();

        // set username
        driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "usernameEditTxt")).sendKeys(username);
        // set password
        driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "passwordEditTxt")).sendKeys(password);
        // synchronize (click OK)

        driver.findElement(By.id("android:id/button1")).click();


        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                driver.findElement(By.id("android:id/button2")).click();
            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }

        }

    }



    @And("^I will press the button Orders in the menu list$")
    public void I_will_press_the_button_Orders_in_the_menu_list() throws Throwable {
        // //*[contains(@text, 'Sign Out')]

        driver.findElement(By.xpath("//android.widget.TextView[@text='Orders']")).click();
    }



    @And("^I will click on the in the user given orders(.*)$")
    public void I_will_click_on_the_in_the_user_given_orders(String orderNo) throws Throwable {

        assert driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")) != null;
        if(driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")) != null){
            driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")).click();
        }else{

        }
    }


    @Then("^I will check if the option (.*) exist in the menu list$")
    public void I_will_check_if_the_option_exist_in_the_menu_list(String MenuItem){


        //get screen Recycler view object
                WebElement RecyclerView = driver.findElement(By.className("android.support.v7.widget.RecyclerView"));

        //get a list of all Linear Layouts above RecyclerView
        List<WebElement> LinearLayout = RecyclerView.findElements(By.className("android.widget.LinearLayout"));


        Assert.assertNotNull(driver.findElement(By.xpath("//android.widget.TextView[@text='"+MenuItem+"']")),"It doesn't exist" );


    }


    @AfterClass
    public static void teardown() throws Exception {
        //driver.quit();
        AndroidSetup_KS.tearDown();
        System.out.println("Ran the after");
    }



}
