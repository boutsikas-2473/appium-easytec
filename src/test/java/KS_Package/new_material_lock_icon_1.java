package KS_Package;

import TEST_CONFIGURATION_FILES.AndroidSetup;
import TEST_CONFIGURATION_FILES.AndroidSetup_KS;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.android.AndroidDriver;
import org.apache.http.util.TextUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by tesse on 5/2/2017.
 */
public class new_material_lock_icon_1 {

    private static AndroidDriver driver;

    /**********************************
     *
     *
     *      If you want to run this test in the Generic Flavor please change the AndroidSetupKS with
     *      AndroidSetup
     *
     *      And some  id's must be change in order to work in Generic Flavor
     *
     */

    //******************* Setting the driver and the capabilities
    @Before
    public void before() {
        try {

            AndroidSetup_KS.deviceCapabilities();

        } catch (Exception e) {
            e.printStackTrace();
        }
        driver = AndroidSetup_KS.getDriver();


    }



    @Given("^I will log_in the  Syncs_Serverss with_KS (.*),(.*),(.*)$")
    public void I_will_log__in_the_Syncs_Serverss_with_KS(String ip, String port, String ssl) {


        driver.findElement(By.xpath("//android.widget.TextView[@text='Settings']")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Account']")).click();

        // set SyncServer IP
        driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "serverAddressEditTxt")).sendKeys(ip);

        // set SyncServer port
        if (!TextUtils.isEmpty(port)) {
            driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "serverPortEditTxt")).sendKeys(port);
        }

        // enable or disable SSL
        if (Boolean.parseBoolean(ssl)) {
            driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "sslChk")).click();
        }

        // save synchronization settings
        driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "save")).click();

        driver.findElement(By.id("android:id/button1")).click();


        driver.findElement(By.id("gr.tessera.easytec.ks:id/clearCredentialsBtn")).click();


        driver.findElement(By.id("android:id/button1")).click();

        // we are the main menu
    }



    @When("^I am going to  sign_in as a valid user with credentials_1  (.*),(.*)$")
    public void I_am_going_to_sign_in_as_a_valid_user_with_credentials_1(String username, String password) throws Throwable {

        // click synchronization action bar button
        driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "sync")).click();

        // set username
        driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "usernameEditTxt")).sendKeys(username);
        // set password
        driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "passwordEditTxt")).sendKeys(password);
        // synchronize (click OK)

        driver.findElement(By.id("android:id/button1")).click();


        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                driver.findElement(By.id("android:id/button2")).click();
            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }

        }

    }



    @And("^I have to Click on the menu button Orders_1$")
    public void I_have_to_Click_on_the_menu_button_Orders_1() throws Throwable {
        // //*[contains(@text, 'Sign Out')]

        driver.findElement(By.xpath("//android.widget.TextView[@text='Orders']")).click();

        Thread.sleep(1000);


    }


    @And("^I will_find and  press  the order with id_1 (.*)$")
    public void I_will_find_and_press_the_order_with_id_1(String orderNo) throws Throwable {
        assert driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")) != null;
        if(driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")) != null){
            driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")).click();
        }else{

        }
    }




    @And("^I will  Click the Menu item Materials_1$")
    public void I_will_Click_the_Menu_item_Materials_1() throws InterruptedException {

        Thread.sleep(2000);

        driver.findElement(By.xpath("//android.widget.TextView[@text='Materials']")).click();

    }


    @And("^I will add a new material and i will sync_1 (.*)$")
    public void I_will_add_a_new_material_and_i_will_sync_1(String Material) throws InterruptedException {

        driver.findElement(By.id("gr.tessera.easytec.ks:id/add_material")).click();
        Thread.sleep(1000);


        driver.findElement(By.xpath("//android.widget.TextView[@text='"+Material+"']")).click();

        Thread.sleep(1000);

        //set quantity

        driver.findElement(By.id("gr.tessera.easytec.ks:id/amountTxt")).click();

        Thread.sleep(1000);

        driver.findElement(By.xpath("//android.widget.Button[@text='10']")).click();

        Thread.sleep(1000);

        driver.findElement(By.id("gr.tessera.easytec.ks:id/okBtn")).click();

        Thread.sleep(1000);

        driver.findElement(By.id("gr.tessera.easytec.ks:id/save")).click();


        Thread.sleep(1500);

        driver.findElement(By.id("android:id/home")).click();

        Thread.sleep(3000);

        driver.findElement(By.id("android:id/home")).click();

        Thread.sleep(2000);

        driver.findElement(By.id("android:id/home")).click();

        Thread.sleep(2000);
        // if running this test in Generic Flavor , use gr.tessera,eaytec:id/sync
        driver.findElement(By.id("gr.tessera.easytec.ks:id/sync")).click();

    }


    @Then("^I will check if the lock icon exist_1 (.*)$")
    public void I_will_check_if_the_lock_icon_exist_1(String OrderNo) throws InterruptedException {



        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                driver.findElement(By.id("android:id/button2")).click();
            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }

        }

        driver.findElement(By.xpath("//android.widget.TextView[@text='Orders']")).click();

        Thread.sleep(1000);


        assert driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+OrderNo+"')]")) != null;
        if(driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+OrderNo+"')]")) != null){
            driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+OrderNo+"')]")).click();
        }else{

        }




        driver.findElement(By.xpath("//android.widget.TextView[@text='Materials']")).click();


        WebElement ListView= driver.findElement(By.className("android.widget.ListView"));

        List<WebElement> List = ListView.findElements(By.className("android.widget.LinearLayout"));

        Assert.assertNotNull("There is not material",List.get(2));


    }



    @AfterClass
    public static void teardown() throws Exception {
        //driver.quit();
        AndroidSetup_KS.tearDown();
        System.out.println("Ran the after");
    }


}
