package KS_Package;

import TEST_CONFIGURATION_FILES.AndroidSetup_KS;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.android.AndroidDriver;
import org.apache.http.util.TextUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by tesse on 4/25/2017.
 */
public class driving_option_txt_changes {

    private static AndroidDriver driver;



    //******************* Setting the driver and the capabilities
    @Before
    public void before() {
        try {
            AndroidSetup_KS.deviceCapabilities();
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver = AndroidSetup_KS.getDriver();


    }



    @Given("^I have to sign into the applications SyncServer  (.*),(.*),(.*)$")
    public void I_have_to_sign_into_the_applications_SyncServer(String ip, String port, String ssl) {


        driver.findElement(By.xpath("//android.widget.TextView[@text='Settings']")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Account']")).click();

        // set SyncServer IP
        driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "serverAddressEditTxt")).sendKeys(ip);

        // set SyncServer port
        if (!TextUtils.isEmpty(port)) {
            driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "serverPortEditTxt")).sendKeys(port);
        }

        // enable or disable SSL
        if (Boolean.parseBoolean(ssl)) {
            driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "sslChk")).click();
        }

        // save synchronization settings
        driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "save")).click();

        driver.findElement(By.id("android:id/button1")).click();


        driver.findElement(By.id("gr.tessera.easytec.ks:id/clearCredentialsBtn")).click();


        driver.findElement(By.id("android:id/button1")).click();

        // we are the main menu
    }



    @When("^I will have to as user to sign in  with credentials  (.*),(.*)$")
    public void I_will_have_to_as_user_to_sign_in_with_credentials(String username, String password) throws Throwable {

        // click synchronization action bar button
        driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "sync")).click();

        // set username
        driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "usernameEditTxt")).sendKeys(username);
        // set password
        driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "passwordEditTxt")).sendKeys(password);
        // synchronize (click OK)

        driver.findElement(By.id("android:id/button1")).click();


        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                driver.findElement(By.id("android:id/button2")).click();
            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }

        }

    }


    @And("^I will press on the Employee list and click (.*)$")
    public void I_will_press_on_the_Employee_list_and_click(String EmployeeName) throws InterruptedException {

        WebElement Spinner  = driver.findElement(By.className("android.widget.Spinner"));

        Spinner.click();

        Thread.sleep(2000);


        List<WebElement> SpinnerCheckedList  =driver.findElement(By.className("android.widget.ListView")).findElements(By.className("android.widget.CheckedTextView"));


        for(int i=0; i<SpinnerCheckedList.size(); i++){

                if(SpinnerCheckedList.get(i).getText().contains(EmployeeName)){
                    SpinnerCheckedList.get(i).click();
                }
        }

        Thread.sleep(2000);





    }



    @Then("^I will check if all functionalities of  Driving Options works$")
    public void I_will_check_if_all_functionalities_of_Driving_Options_works() throws InterruptedException {


        //click on the StartDriving option

        List<WebElement> LinearLayouts = driver.findElement(By.className("android.widget.ListView")).findElements(By.className("android.widget.LinearLayout"));

        LinearLayouts.get(0).click();

        Thread.sleep(2000);

        //check if the prefic (F) exist

        LinearLayouts.get(2).click();

        Thread.sleep(1000);

        Assert.assertTrue("The (F) does not exist",driver.findElement(By.id("android:id/action_bar_title")).getText().contains("(F)") );


        // go into the first menu

        driver.findElement(By.id("android:id/home")).click();


        Thread.sleep(1000);


        //click on the StartDriving ( now has StopDriving ) wait for 1sec and click StartResting

        LinearLayouts.get(0).click();

        Thread.sleep(1000);


        LinearLayouts.get(1).click();


        //check if the prefic (P) exist

        LinearLayouts.get(2).click();



        Thread.sleep(1000);

        Assert.assertTrue("The (F) does not exist",driver.findElement(By.id("android:id/action_bar_title")).getText().contains("(P)") );



    }





}
