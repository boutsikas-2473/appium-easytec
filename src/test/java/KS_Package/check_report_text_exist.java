package KS_Package;

import TEST_CONFIGURATION_FILES.AndroidSetup_KS;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.android.AndroidDriver;
import org.apache.http.util.TextUtils;
import org.junit.AfterClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import java.util.List;

/**
 * Created by tesse on 4/26/2017.
 */
public class check_report_text_exist {


    private static AndroidDriver driver;
    private WebElement ReportWebElement=null;
    private boolean ReportTextExist=false;
    private int Counter=0;


    //******************* Setting the driver and the capabilities
    @Before
    public void before() {
        try {
            AndroidSetup_KS.deviceCapabilities();
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver = AndroidSetup_KS.getDriver();


    }



    @Given("^When i will have sign in  SyncServer with  (.*),(.*),(.*)$")
    public void When_i_will_have_sign_in_SyncServer_with(String ip, String port, String ssl) {


        driver.findElement(By.xpath("//android.widget.TextView[@text='Settings']")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Account']")).click();

        // set SyncServer IP
        driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "serverAddressEditTxt")).sendKeys(ip);

        // set SyncServer port
        if (!TextUtils.isEmpty(port)) {
            driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "serverPortEditTxt")).sendKeys(port);
        }

        // enable or disable SSL
        if (Boolean.parseBoolean(ssl)) {
            driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "sslChk")).click();
        }

        // save synchronization settings
        driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "save")).click();

        driver.findElement(By.id("android:id/button1")).click();


        driver.findElement(By.id("gr.tessera.easytec.ks:id/clearCredentialsBtn")).click();


        driver.findElement(By.id("android:id/button1")).click();

        // we are the main menu
    }



    @When("^I have successfully sign in  application with credentials  (.*),(.*)$")
    public void I_have_successfully_sign_in_application_with_credentials(String username, String password) throws Throwable {

        // click synchronization action bar button
        driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "sync")).click();

        // set username
        driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "usernameEditTxt")).sendKeys(username);
        // set password
        driver.findElement(By.id(AndroidSetup_KS.app_package_name + ":id/" + "passwordEditTxt")).sendKeys(password);
        // synchronize (click OK)

        driver.findElement(By.id("android:id/button1")).click();


        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                driver.findElement(By.id("android:id/button2")).click();
            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }

        }

    }



    @And("^I will press the menu item Orders in the menu list$")
    public void I_will_press_the_menu_item_Orders_in_the_menu_list() throws Throwable {
        // //*[contains(@text, 'Sign Out')]

        driver.findElement(By.xpath("//android.widget.TextView[@text='Orders']")).click();
    }



    @And("^I click on one order with name(.*)$")
    public void  I_click_on_one_order_with_name(String orderNo) throws Throwable {

        // click on the filters panel
        driver.findElement(By.id("gr.tessera.easytec.ks:id/filtersToggleTxt")).click();

        Thread.sleep(1000);



        assert driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")) != null;
        if(driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")) != null){
            driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")).click();
        }else{
                    System.out.print("The order does not exist");
        }



        //close Briefing panel
        driver.findElement(By.id("gr.tessera.easytec.ks:id/briefingToggleTxt")).click();


        //get screen Recycler view object
        WebElement RecyclerView = driver.findElement(By.className("android.support.v7.widget.RecyclerView"));

        //get a list of all Linear Layouts above RecyclerView
        List<WebElement> RelativeLayout = RecyclerView.findElements(By.className("android.widget.RelativeLayout"));



        //check if exist a layout with text include Report + text


        do{
            if(RelativeLayout.get(Counter).findElement(By.className("android.widget.TextView")).getText().contains("Report")){
                ReportWebElement = RelativeLayout.get(Counter).findElement(By.className("android.widget.TextView"));
                ReportTextExist=!ReportTextExist;
                Counter++;
            }else{

                Counter++;
            }
        }while(!ReportTextExist);


        Assert.assertTrue(ReportTextExist,"is false");


        ReportWebElement.click();


        Thread.sleep(1000);


        //clear the boolean value
                ReportTextExist=!ReportTextExist;
                Counter=0;


    }


    @Then("^I will go back to orders menu  and i will pick (.*)$")
    public void I_will_go_back_to_orders_menu_and_i_will_pick(String OrderNo) throws InterruptedException {

            Thread.sleep(1000);

            driver.findElement(By.id("android:id/home")).click();

            Thread.sleep(1000);

             driver.findElement(By.id("android:id/home")).click();

        assert driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+OrderNo+"')]")) != null;
        if(driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+OrderNo+"')]")) != null){
            driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+OrderNo+"')]")).click();
        }else{
            System.out.print("The order does not exist");
        }



        Thread.sleep(1000);


        //close Briefing panel
        driver.findElement(By.id("gr.tessera.easytec.ks:id/briefingToggleTxt")).click();


        //get screen Recycler view object
        WebElement RecyclerView = driver.findElement(By.className("android.support.v7.widget.RecyclerView"));

        //get a list of all Linear Layouts above RecyclerView
        List<WebElement> RelativeLayout = RecyclerView.findElements(By.className("android.widget.RelativeLayout"));



        //check if exist a layout with text include Report + text

        Thread.sleep(2000);
        do{

            if(RelativeLayout.get(Counter).findElement(By.className("android.widget.TextView")).getText().contains("Report")){
                ReportTextExist=!ReportTextExist;
                Counter++;
            }else{
                Counter++;
            }

        }while(Counter<RelativeLayout.size());


        Assert.assertFalse(ReportTextExist,"is false");


        Thread.sleep(1000);




    }




    @AfterClass
    public static void teardown() throws Exception {
        //driver.quit();
        AndroidSetup_KS.tearDown();
        System.out.println("Ran the after");
    }





}
