package Omexon_Package;

import TEST_CONFIGURATION_FILES.AndroidSetup_OMEXON;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.android.AndroidDriver;
import org.apache.http.util.TextUtils;
import org.openqa.selenium.By;
import org.testng.Assert;

/**
 * Created by tesse on 5/18/2017.
 */
public class check_nullable_employees_startdate_finishdate_error {


    private static AndroidDriver driver;
    private static String CurrentActivity = "ParticipantActivity";

    //******************* Setting the driver and the capabilities
    @Before
    public void before() {
        try {

            AndroidSetup_OMEXON.deviceCapabilities();
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver = AndroidSetup_OMEXON.getDriver();


    }



    @Given("^My first step is to sign in the apps server SyncServer with creds (.*),(.*),(.*)$")
    public void My_first_step_is_to_sign_in_the_apps_server_SyncServer_with_creds(String ip, String port, String ssl) {


        driver.findElement(By.xpath("//android.widget.TextView[@text='Settings']")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Account']")).click();

        // set SyncServer IP
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "serverAddressEditTxt")).sendKeys(ip);

        // set SyncServer port
        if (!TextUtils.isEmpty(port)) {
            driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "serverPortEditTxt")).sendKeys(port);
        }

        // enable or disable SSL
        if (Boolean.parseBoolean(ssl)) {
            driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "sslChk")).click();
        }

        // save synchronization settings
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "save")).click();

        driver.findElement(By.id("android:id/button1")).click();


        driver.findElement(By.id("gr.tessera.easytec.omexom:id/clearCredentialsBtn")).click();


        driver.findElement(By.id("android:id/button1")).click();

        // we are the main menu
    }



    @When("^I will go back and log_in as valid user with data (.*),(.*)$")
    public void I_will_go_back_and_log_in_as_valid_user_with_data(String username, String password) throws Throwable {

        // click synchronization action bar button
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "sync")).click();

        // set username
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "usernameEditTxt")).sendKeys(username);
        // set password
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "passwordEditTxt")).sendKeys(password);
        // synchronize (click OK)

        driver.findElement(By.id("android:id/button1")).click();


        //Thread.sleep(35000);
        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                driver.findElement(By.id("android:id/button2")).click();
            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }

        }

    }


    @And("^I will click the (.*) button in the screen and  I will find the order i want with id (.*) and click it$")
    public void I_will_click_the_Order_button_in_the_screen_and_I_will_find_the_order_i_want_with_id_andclick_it(String MenuOption,String orderNo) throws Throwable {
        // //*[contains(@text, 'Sign Out')]
        driver.findElement(By.xpath("//android.widget.TextView[@text='"+MenuOption+"']")).click();

        assert driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")) != null;
        if(driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")) != null){
            driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")).click();
        }else{

        }

    }




    @And("^I will click on the option in the list with name (.*)$")
    public void I_will_click_on_the_option_in_the_list_with_name_Employees(String MenuOption) throws InterruptedException {


        if (driver.findElements(By.xpath("//android.widget.TextView[@text='" + MenuOption + "']")).size() != 0) {
            driver.findElement(By.xpath("//android.widget.TextView[@text='" + MenuOption + "']")).click();
        } else {
            throw new NullPointerException("The is none Hours and Costs Object in Ui panel");
        }
    }


    @Then("^I will just click the finish button and check if an error has occured$")
    public void I_will_just_click_the_finish_button_and_check_if_an_error_has_occured() throws InterruptedException {

          driver.findElement(By.id("gr.tessera.easytec.omexom:id/finish")).click();

          System.out.println(driver.currentActivity());

        Assert.assertTrue(driver.currentActivity().contains(CurrentActivity));




    }






    }
