package Omexon_Package;

import TEST_CONFIGURATION_FILES.AndroidSetup_OMEXON;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.android.AndroidDriver;
import org.apache.http.util.TextUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.List;

/**
 * Created by tesse on 5/18/2017.
 */
public class check_description_spinner {


    private static AndroidDriver driver;
    private static String CurrentActivity[] = new String[]{"prj","descr"};

    //******************* Setting the driver and the capabilities
    @Before
    public void before() {
        try {

            AndroidSetup_OMEXON.deviceCapabilities();
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver = AndroidSetup_OMEXON.getDriver();


    }



    @Given("^First step is to log in the application syncServer with creds (.*),(.*),(.*)$")
    public void First_step_is_to_log_in_the_application_syncServer_with_creds(String ip, String port, String ssl) {


        driver.findElement(By.xpath("//android.widget.TextView[@text='Settings']")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Account']")).click();

        // set SyncServer IP
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "serverAddressEditTxt")).sendKeys(ip);

        // set SyncServer port
        if (!TextUtils.isEmpty(port)) {
            driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "serverPortEditTxt")).sendKeys(port);
        }

        // enable or disable SSL
        if (Boolean.parseBoolean(ssl)) {
            driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "sslChk")).click();
        }

        // save synchronization settings
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "save")).click();

        driver.findElement(By.id("android:id/button1")).click();


        driver.findElement(By.id("gr.tessera.easytec.omexom:id/clearCredentialsBtn")).click();


        driver.findElement(By.id("android:id/button1")).click();

        // we are the main menu
    }



    @When("^I must log_in as a valid user in order to see the order with (.*),(.*)$")
    public void I_must_log_in_as_a_valid_user_in_order_to_see_the_order_with(String username, String password) throws Throwable {

        // click synchronization action bar button
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "sync")).click();

        // set username
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "usernameEditTxt")).sendKeys(username);
        // set password
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "passwordEditTxt")).sendKeys(password);
        // synchronize (click OK)

        driver.findElement(By.id("android:id/button1")).click();


        //Thread.sleep(35000);
        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                driver.findElement(By.id("android:id/button2")).click();
            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }

        }

    }


    @And("^I must find the button with text (.*) I must find a random order with id (.*)$")
    public void I_will_click_the_Order_button_in_the_screen_and_I_will_find_the_order_i_want_with_id_andclick_it(String MenuOption,String orderNo) throws Throwable {
        // //*[contains(@text, 'Sign Out')]
        driver.findElement(By.xpath("//android.widget.TextView[@text='"+MenuOption+"']")).click();

        assert driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")) != null;
        if(driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")) != null){
            driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")).click();
        }else{

        }

    }




    @And("^I will click on the option (.*) and click it$")
    public void I_will_click_on_the_option_in_the_list_with_name_Employees(String MenuOption) throws InterruptedException {


        if (driver.findElements(By.xpath("//android.widget.TextView[@text='" + MenuOption + "']")).size() != 0) {
            driver.findElement(By.xpath("//android.widget.TextView[@text='" + MenuOption + "']")).click();
        } else {
            throw new NullPointerException("The is none Description Object in Ui panel");
        }
    }



    @Then("^I will check the values of the spinner$")
    public void I_will_check_the_values_of_the_spinner() throws InterruptedException {

        int ListViewCounter = driver.findElement(By.className("android.widget.ListView")).findElements(By.className("android.widget.LinearLayout")).size();

        driver.findElement(By.className("android.widget.Spinner")).click();


         List<WebElement> CheckedViewList = driver.findElements(By.className("android.widget.CheckedTextView"));

        //click the first option woth name = prj
            driver.findElement(By.xpath("//android.widget.CheckedTextView[contains(@text,'"+CurrentActivity[0]+"')]")).click();

            Thread.sleep(3000);

        Assert.assertTrue(driver.findElement(By.className("android.widget.ListView")).findElements(By.className("android.widget.LinearLayout")).size()-1==1);

        driver.findElement(By.className("android.widget.Spinner")).click();

        //click the first option woth name = prj
        driver.findElement(By.xpath("//android.widget.CheckedTextView[contains(@text,'"+CurrentActivity[1]+"')]")).click();

        Thread.sleep(3000);

        Assert.assertTrue(driver.findElement(By.className("android.widget.ListView")).findElements(By.className("android.widget.LinearLayout")).size()-1==2);

    }




}
