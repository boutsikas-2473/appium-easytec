package Omexon_Package;

import TEST_CONFIGURATION_FILES.AndroidSetup;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import org.apache.http.util.TextUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by tesse on 4/21/2017.
 */
public class save_favourite_employee {

    private static AndroidDriver driver;
    private String PICKED_EMPLOYEE_NAME=null;


    //******************* Setting the driver and the capabilities
    @Before
    public void before() {
        try {

            AndroidSetup.deviceCapabilities();
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver = AndroidSetup.getDriver();


    }



    @Given("^First user must log-in in the Server (.*),(.*),(.*)$")
    public void First_user_must_log_in_in_the_Server(String ip, String port, String ssl) {


        driver.findElement(By.xpath("//android.widget.TextView[@text='Settings']")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Account']")).click();

        // set SyncServer IP
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "serverAddressEditTxt")).sendKeys(ip);

        // set SyncServer port
        if (!TextUtils.isEmpty(port)) {
            driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "serverPortEditTxt")).sendKeys(port);
        }

        // enable or disable SSL
        if (Boolean.parseBoolean(ssl)) {
            driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "sslChk")).click();
        }

        // save synchronization settings
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "save")).click();

        driver.findElement(By.id("android:id/button1")).click();


        driver.findElement(By.id("gr.tessera.easytec:id/clearCredentialsBtn")).click();


        driver.findElement(By.id("android:id/button1")).click();

        // we are the main menu
    }



    @When("^The user has been successfully sign-in as a valid user with (.*),(.*)$")
    public void The_user_has_been_successfully_sign_in_as_a_valid_user_with(String username, String password) throws Throwable {

        // click synchronization action bar button
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "sync")).click();

        // set username
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "usernameEditTxt")).sendKeys(username);
        // set password
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "passwordEditTxt")).sendKeys(password);
        // synchronize (click OK)

        driver.findElement(By.id("android:id/button1")).click();


        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                driver.findElement(By.id("android:id/button2")).click();
            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }

        }

    }



    @And("^I press the Employee tab in the first menu list$")
    public void I_press_the_Employee_tab_in_the_first_menu_list() throws Throwable {
        // //*[contains(@text, 'Sign Out')]

        driver.findElement(By.xpath("//android.widget.TextView[@text='Employees']")).click();
    }



    @And("^I long press the employee which i want single or multiple$")
    public void I_long_press_the_employee_which_i_want_single_or_multiple(){


        WebElement ListView = driver.findElement(By.className("android.widget.ListView"));
        List<WebElement> LinearLayoutList = ListView.findElements(By.className("android.widget.LinearLayout"));


        TouchAction LongPressAction = new TouchAction((MobileDriver)driver);

        //Long press the first option on the list

        LongPressAction.longPress(LinearLayoutList.get(0),3000).release();

        LongPressAction.perform();



        // click on the favorite button

        





















    }






}
