package Omexon_Package;

import TEST_CONFIGURATION_FILES.AndroidSetup_OMEXON;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.android.AndroidDriver;
import org.apache.http.util.TextUtils;
import org.openqa.selenium.By;
import org.testng.Assert;

/**
 * Created by tesse on 5/18/2017.
 */
public class check_default_equipment_text_visibility {

    private static AndroidDriver driver;

    //******************* Setting the driver and the capabilities
    @Before
    public void before() {
        try {

            AndroidSetup_OMEXON.deviceCapabilities();
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver = AndroidSetup_OMEXON.getDriver();


    }



    @Given("^I have must sign in the application server with name Syncserver and credentials (.*),(.*),(.*)$")
    public void I_have_must_sign_in_the_application_server_with_name_Syncserver_and_credentials(String ip, String port, String ssl) {


        driver.findElement(By.xpath("//android.widget.TextView[@text='Settings']")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Account']")).click();

        // set SyncServer IP
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "serverAddressEditTxt")).sendKeys(ip);

        // set SyncServer port
        if (!TextUtils.isEmpty(port)) {
            driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "serverPortEditTxt")).sendKeys(port);
        }

        // enable or disable SSL
        if (Boolean.parseBoolean(ssl)) {
            driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "sslChk")).click();
        }

        // save synchronization settings
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "save")).click();

        driver.findElement(By.id("android:id/button1")).click();


        driver.findElement(By.id("gr.tessera.easytec.omexom:id/clearCredentialsBtn")).click();


        driver.findElement(By.id("android:id/button1")).click();

        // we are the main menu
    }



    @When("^I as a valid User will have to log-in  successfully  with valid details (.*),(.*)$")
    public void I_as_a_valid_User_will_have_to_log_in_successfully_with_valid_details(String username, String password) throws Throwable {

        // click synchronization action bar button
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "sync")).click();

        // set username
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "usernameEditTxt")).sendKeys(username);
        // set password
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "passwordEditTxt")).sendKeys(password);
        // synchronize (click OK)

        driver.findElement(By.id("android:id/button1")).click();


        //Thread.sleep(35000);
        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                driver.findElement(By.id("android:id/button2")).click();
            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }

        }

    }


    @And("^I will find in the Orders in the secondscreen with id (.*) and click it$")
    public void I_will_find_in_the_Orders_in_the_secondscreen_with_id(String orderNo) throws Throwable {
        // //*[contains(@text, 'Sign Out')]
        driver.findElement(By.xpath("//android.widget.TextView[@text='Orders']")).click();

        assert driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")) != null;
        if(driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")) != null){
            driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")).click();
        }else{

        }

    }




    @And("^I will find the menu option (.*) and add a new salary as a team$")
    public void I_will_click_the_menu_option_from_the_list_view_with_name_Hours_and_Costs(String MenuOption) throws InterruptedException {


        if(driver.findElements(By.xpath("//android.widget.TextView[@text='"+MenuOption+"']")).size()!=0) {
            driver.findElement(By.xpath("//android.widget.TextView[@text='"+MenuOption+"']")).click();
        }else{
            throw new NullPointerException("The is none Hours and Costs Object in Ui panel");
        }



        //click to add  a new salary
        driver.findElement(By.id("gr.tessera.easytec.omexom:id/add_hours_expand")).click();

        Thread.sleep(3000);

        //click to set that this salary is as a team
        driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'Team')]")).click();

        Thread.sleep(3000);

        // check the field where the favorite employees names shows



    }




    @Then("^I will check if the name of my favorite equipment has the name (.*)$")
    public void I_will_check_if_the_name_of_my_favorite_employee_shows(String DefaultEquipment){


        try {
            Assert.assertTrue(driver.findElement(By.id("gr.tessera.easytec.omexom:id/equipment_picker")).getText().matches(DefaultEquipment));
        }catch(Exception e){

            Assert.assertTrue(false,"The Element with this name doesnt exist");
        }



    }





}
