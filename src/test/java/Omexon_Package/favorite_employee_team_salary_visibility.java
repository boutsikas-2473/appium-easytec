package Omexon_Package;

import TEST_CONFIGURATION_FILES.AndroidSetup_OMEXON;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import org.apache.http.util.TextUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.List;

/**
 * Created by tesse on 5/18/2017.
 */
public class favorite_employee_team_salary_visibility {
    private static AndroidDriver driver;
    private String PICKED_EMPLOYEE_NAME="J.Bond";


    //******************* Setting the driver and the capabilities
    @Before
    public void before() {
        try {

            AndroidSetup_OMEXON.deviceCapabilities();
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver = AndroidSetup_OMEXON.getDriver();


    }



    @Given("^I have must sign in the apps server with name SyncServer and creds (.*),(.*),(.*)$")
    public void I_have_must_sign_in_the_apps_server_with_name_SyncServer_and_creds(String ip, String port, String ssl) {


        driver.findElement(By.xpath("//android.widget.TextView[@text='Settings']")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Account']")).click();

        // set SyncServer IP
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "serverAddressEditTxt")).sendKeys(ip);

        // set SyncServer port
        if (!TextUtils.isEmpty(port)) {
            driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "serverPortEditTxt")).sendKeys(port);
        }

        // enable or disable SSL
        if (Boolean.parseBoolean(ssl)) {
            driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "sslChk")).click();
        }

        // save synchronization settings
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "save")).click();

        driver.findElement(By.id("android:id/button1")).click();


        driver.findElement(By.id("gr.tessera.easytec.omexom:id/clearCredentialsBtn")).click();


        driver.findElement(By.id("android:id/button1")).click();

        // we are the main menu
    }



    @When("^I as a valid user will have successfully log-in the application with valid details (.*),(.*)$")
    public void I_as_a_valid_user_will_have_successfully_log_in_the_application_with_valid_details(String username, String password) throws Throwable {

        // click synchronization action bar button
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "sync")).click();

        // set username
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "usernameEditTxt")).sendKeys(username);
        // set password
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "passwordEditTxt")).sendKeys(password);
        // synchronize (click OK)

        driver.findElement(By.id("android:id/button1")).click();


        //Thread.sleep(35000);
        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                driver.findElement(By.id("android:id/button2")).click();
            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }

        }

    }



    @And("^I will click on the first menu the option with name (.*)$")
    public void I_press_the_Employee_tab_in_the_first_menu_list(String MenuOptions) throws Throwable {
        // //*[contains(@text, 'Sign Out')]

        driver.findElement(By.xpath("//android.widget.TextView[@text='"+MenuOptions+"']")).click();
    }



    @And("^I will make an employee with name J.Bond a favorite employee$")
    public void I_will_make_an_employee_with_name_Bond_a_favorite_employee() throws InterruptedException {


        WebElement ListView = driver.findElement(By.className("android.widget.ListView"));
        List<WebElement> LinearLayoutList = ListView.findElements(By.className("android.widget.LinearLayout"));


        TouchAction LongPressAction = new TouchAction((MobileDriver)driver);

        //Long press the first option on the list

        PICKED_EMPLOYEE_NAME  = LinearLayoutList.get(0).findElement(By.className("android.widget.TextView")).getText();

        LongPressAction.longPress(LinearLayoutList.get(0),3000).release();

        LongPressAction.perform();


        // click on the Favourite button

        driver.findElement(By.id("gr.tessera.easytec.omexom:id/fav")).click();
        Thread.sleep(2000);

        driver.findElement(By.id("android:id/home")).click();

    }



    @And("^I will find the order in the second screen with id (.*) and click it$")
    public void I_will_find_the_order_in_he_second_screen_with_id__and_click_it(String orderNo) throws Throwable {
        // //*[contains(@text, 'Sign Out')]
        driver.findElement(By.xpath("//android.widget.TextView[@text='Orders']")).click();

        assert driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")) != null;
        if(driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")) != null){
            driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")).click();
        }else{

        }

    }




    @And("^I will find the menu option in the list with name (.*) and add a new salary as a team$")
    public void I_will_click_the_menu_option_from_the_list_view_with_name_Hours_and_Costs(String MenuOption) throws InterruptedException {


        if(driver.findElements(By.xpath("//android.widget.TextView[@text='"+MenuOption+"']")).size()!=0) {
            driver.findElement(By.xpath("//android.widget.TextView[@text='"+MenuOption+"']")).click();
        }else{
            throw new NullPointerException("The is none Hours and Costs Object in Ui panel");
        }



        //click to add  a new salary
          driver.findElement(By.id("gr.tessera.easytec.omexom:id/add_hours_expand")).click();

        Thread.sleep(3000);

        //click to set that this salary is as a team
            driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'Team')]")).click();

        Thread.sleep(3000);

        // check the field where the favorite employees names shows

        driver.findElement(By.id("gr.tessera.easytec.omexom:id/employee_picker")).click();

        Thread.sleep(3000);



    }




    @Then("^I will check if the name of my favorite employee shows$")
    public void I_will_check_if_the_name_of_my_favorite_employee_shows(){



        try {
            Assert.assertTrue(driver.findElement(By.xpath("//android.widget.CheckedTextView[contains(@text,'" + PICKED_EMPLOYEE_NAME + "')]")).getText().matches(PICKED_EMPLOYEE_NAME));
        }catch(Exception e){

            Assert.assertTrue(false,"The Element with this name doesnt exist");
        }



    }






}
