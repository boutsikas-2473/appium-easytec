package Omexon_Package;

import TEST_CONFIGURATION_FILES.AndroidSetup;
import TEST_CONFIGURATION_FILES.AndroidSetup_OMEXON;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import io.appium.java_client.android.AndroidDriver;
import org.apache.http.util.TextUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by tesse on 4/24/2017.
 */
public class order_sorting_menu {

    private static AndroidDriver driver;
    private ArrayList<String> MenuOptions  = new ArrayList();
    private int menuOptionCounter=0;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd");
    private Date StartDate,FinishDate;











    //******************* Setting the driver and the capabilities
    @Before
    public void before() {
        try {

            AndroidSetup_OMEXON.deviceCapabilities();
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver = AndroidSetup_OMEXON.getDriver();


    }



    @Given("^I have to sign in the app Server (.*),(.*),(.*)$")
    public void I_have_to_sign_in_the_app_Server(String ip, String port, String ssl) {


        driver.findElement(By.xpath("//android.widget.TextView[@text='Settings']")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Account']")).click();

        // set SyncServer IP
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "serverAddressEditTxt")).sendKeys(ip);

        // set SyncServer port
        if (!TextUtils.isEmpty(port)) {
            driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "serverPortEditTxt")).sendKeys(port);
        }

        // enable or disable SSL
        if (Boolean.parseBoolean(ssl)) {
            driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "sslChk")).click();
        }

        // save synchronization settings
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "save")).click();

        driver.findElement(By.id("android:id/button1")).click();


        driver.findElement(By.id("gr.tessera.easytec.omexom:id/clearCredentialsBtn")).click();


        driver.findElement(By.id("android:id/button1")).click();

        // we are the main menu
    }



    @When("^I have been successfully get in as a valid user with (.*),(.*)$")
    public void I_have_been_successfully_get_in_as_a_valid_user_with(String username, String password) throws Throwable {

        // click synchronization action bar button
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "sync")).click();

        // set username
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "usernameEditTxt")).sendKeys(username);
        // set password
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "passwordEditTxt")).sendKeys(password);
        // synchronize (click OK)

        driver.findElement(By.id("android:id/button1")).click();


        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                driver.findElement(By.id("android:id/button2")).click();
            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }

        }

    }



    @And("^I have to press the order option from the menu")
    public void I_have_press_the_order_option_from_the_menu() throws Throwable {
        // //*[contains(@text, 'Sign Out')]

        driver.findElement(By.xpath("//android.widget.TextView[@text='Orders']")).click();
    }



    @And("^I click on the sorting menu with chosing all options in the list$")
    public void I_click_on_the_sorting_menu_with_chosing_all_options_in_the_list() throws InterruptedException, ParseException {



    // get the order list layout before we start
        WebElement OrdersListView = driver.findElement(By.className("android.widget.ListView"));
        List<WebElement> OrderLayoutList = OrdersListView.findElements(By.className("android.widget.LinearLayout"));


     // find the view where the sort button on the menu is
        WebElement View  =  driver.findElement(By.className("android.view.View"));
        List<WebElement> RelativeLayoutsList = View.findElements(By.className("android.widget.FrameLayout"));


        // first erase hte filter panel

        driver.findElement(By.id("gr.tessera.easytec.omexom:id/filtersToggleTxt")).click();



        //find the sorting button from the menu

        RelativeLayoutsList.get(0).findElement(By.id("gr.tessera.easytec.omexom:id/sort")).click();



        Thread.sleep(2000);


        //Get the sorting menu ListView

        WebElement ListView = driver.findElement(By.className("android.widget.ListView"));

        List<WebElement> LinearLayouts = ListView.findElements(By.className("android.widget.LinearLayout"));


        Thread.sleep(2000);


        while(menuOptionCounter!=LinearLayouts.size()){


            // save the option name in th array

            MenuOptions.add(LinearLayouts.get(menuOptionCounter).findElement(By.className("android.widget.TextView")).getText());

            // click it and wait for action

            System.out.print("Order Names  : "  +MenuOptions.get(menuOptionCounter)   +"\n" );


            LinearLayouts.get(menuOptionCounter).click();



            if(MenuOptions.get(menuOptionCounter).contains("Start Date (A-Z)")){

                //check the first two order's ,  if they starts from the the lowest start day to the max start day in a month

                String StartDateString = OrderLayoutList.get(0).findElement(By.id("gr.tessera.easytec.omexom:id/orderStartDateTxt")).getText();

                String[]  StartDate = StartDateString.split("/");


                String FinishDateString = OrderLayoutList.get(2).findElement(By.id("gr.tessera.easytec.omexom:id/orderStartDateTxt")).getText();

                String[]  FinishDate = FinishDateString.split("/");

                System.out.print("StartDay " + StartDate[0] +" : " + " Finish Date "+ FinishDate[0] +"\n");


                    // check two orders in a row has the same start day , in order to check the month
                     if(StartDate[0].matches(FinishDate[0])){

                        Assert.assertTrue("The Finish Date is not higher than the last day" , FinishDate[1].compareToIgnoreCase(StartDate[1]) >0  );

                     }else{

                    Assert.assertTrue("The Finish Date is not higher than the last day" , FinishDate[0].compareToIgnoreCase(StartDate[0])>0);
                     }

                // increase the counter
                menuOptionCounter++;

                //push the Sort menu button
                RelativeLayoutsList.get(0).findElement(By.id("gr.tessera.easytec.omexom:id/sort")).click();


            }else if(MenuOptions.get(menuOptionCounter).contains("Start Date (Z-A)")){
                //check the first two order's ,  if they starts from the the lowest start day to the max start day in a month

                Thread.sleep(2000);


                String StartDateString = OrderLayoutList.get(0).findElement(By.id("gr.tessera.easytec.omexom:id/orderStartDateTxt")).getText();

                String[]  StartDate1 = StartDateString.split("/");


                String FinishDateString = OrderLayoutList.get(2).findElement(By.id("gr.tessera.easytec.omexom:id/orderStartDateTxt")).getText();


                String[]  FinishDate1 = FinishDateString.split("/");



                System.out.print("StartDay " + StartDate1[0] +" : " + " Finish Date "+ FinishDate1[0] +"\n");


                // check two orders in a row has the same start day , in order to check the month
                if(StartDate1[0].matches(FinishDate1[0])){

                    Assert.assertTrue("The Finish Date is not higher than the last day" , FinishDate1[1].compareToIgnoreCase(StartDate1[1]) <0  );

                }else{

                    Assert.assertTrue("The Finish Date is not higher than the last day" , FinishDate1[0].compareToIgnoreCase(StartDate1[0])<0);
                }

                // increase the counter
                menuOptionCounter++;

                //push the Sort menu button
                RelativeLayoutsList.get(0).findElement(By.id("gr.tessera.easytec.omexom:id/sort")).click();

            }else if(MenuOptions.get(menuOptionCounter).contains("Address (A-Z)")){

                //check the first two order's , if they starts from the the lowest (alphabetically) Address to  the max Address
                Thread.sleep(2000);

                String AddressStart = OrderLayoutList.get(0).findElement(By.id("gr.tessera.easytec.omexom:id/orderAddressTxt")).getText();

                Thread.sleep(2000);

                String AddressFinish = OrderLayoutList.get(2).findElement(By.id("gr.tessera.easytec.omexom:id/orderAddressTxt")).getText();


                // check the assertion
                Assert.assertTrue("The Start Address  is not lower (alphabetically_)  than the last Address" ,AddressStart.compareToIgnoreCase(AddressFinish)<0);

                // increase the counter
                menuOptionCounter++;

                //push the Sort menu button
                RelativeLayoutsList.get(0).findElement(By.id("gr.tessera.easytec.omexom:id/sort")).click();


            }else if(MenuOptions.get(menuOptionCounter).contains("Address (Z-A)")){


                //check the first two orders ,   if they starts from the the last (alphabetically) Address to  the lowest Address

                Thread.sleep(2000);

                String AddressStart = OrderLayoutList.get(0).findElement(By.id("gr.tessera.easytec.omexom:id/orderAddressTxt")).getText();

                Thread.sleep(2000);

                String AddressFinish = OrderLayoutList.get(2).findElement(By.id("gr.tessera.easytec.omexom:id/orderAddressTxt")).getText();

                // check the assertion
                Assert.assertTrue("The Start Address is not higher than the Last  Address" ,AddressStart.compareToIgnoreCase(AddressFinish)>0);


                // increase the counter
                menuOptionCounter++;

                //push the Sort menu button
                RelativeLayoutsList.get(0).findElement(By.id("gr.tessera.easytec.omexom:id/sort")).click();



            }else if(MenuOptions.get(menuOptionCounter).contains("Order No (A-Z)")){

                //check the first two order  if the first order has lower alphabetically name than the next order

                Thread.sleep(2000);

                String AddressStart = OrderLayoutList.get(0).findElement(By.id("gr.tessera.easytec.omexom:id/orderIdTxt")).getText();

                Thread.sleep(2000);

                String AddressFinish = OrderLayoutList.get(2).findElement(By.id("gr.tessera.easytec.omexom:id/orderIdTxt")).getText();

                // check the assertion
                Assert.assertTrue("The Finish Date is not higher than the last day" ,AddressStart.compareToIgnoreCase(AddressFinish)<0);


                // increase the counter
                menuOptionCounter++;

                //push the Sort menu button
                RelativeLayoutsList.get(0).findElement(By.id("gr.tessera.easytec.omexom:id/sort")).click();



            }else if(MenuOptions.get(menuOptionCounter).contains("Order No (Z-A)")){

                //check the first two order to check if the order starts from the the lowest start day to the max start day in a month

                Thread.sleep(2000);

                String AddressStart = OrderLayoutList.get(0).findElement(By.id("gr.tessera.easytec.omexom:id/orderIdTxt")).getText();

                Thread.sleep(2000);

                String AddressFinish = OrderLayoutList.get(2).findElement(By.id("gr.tessera.easytec.omexom:id/orderIdTxt")).getText();

                // check the assertion
                Assert.assertTrue("The Finish Date is not higher than the last day" ,AddressStart.compareToIgnoreCase(AddressFinish)>0);


                // increase the counter
                menuOptionCounter++;


                //push the Sort menu button
                RelativeLayoutsList.get(0).findElement(By.id("gr.tessera.easytec.omexom:id/sort")).click();



            }else{

                // increase the counter
                menuOptionCounter++;

                //push the Sort menu button
                RelativeLayoutsList.get(0).findElement(By.id("gr.tessera.easytec.omexom:id/sort")).click();
            }


        }//end of while




        //*TODO
        // check why when the order list load's the layouts list starts from 0 and jumps into 2 (skipping the [1] layout)
        //





    }


    @AfterClass
    public static void teardown() throws Exception {
        //driver.quit();
        AndroidSetup.tearDown();
        System.out.println("Ran the after");
    }


}
