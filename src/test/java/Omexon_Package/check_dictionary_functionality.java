package Omexon_Package;

import TEST_CONFIGURATION_FILES.AndroidSetup_OMEXON;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.android.AndroidDriver;
import org.apache.http.util.TextUtils;
import org.openqa.selenium.By;
import org.testng.Assert;

/**
 * Created by tesse on 5/22/2017.
 */
public class check_dictionary_functionality {


    private static AndroidDriver driver;
    private static StringBuilder StringBuilder = new StringBuilder();

    //******************* Setting the driver and the capabilities
    @Before
    public void before() {
        try {

            AndroidSetup_OMEXON.deviceCapabilities();
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver = AndroidSetup_OMEXON.getDriver();


    }



    @Given("^From the start of the test i must log in the Servers SyncServer with (.*),(.*),(.*)$")
    public void From_the_start_of_the_test_i_must_log_in_the_Servers_SyncServer_with(String ip, String port, String ssl) {


        driver.findElement(By.xpath("//android.widget.TextView[@text='Settings']")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Account']")).click();

        // set SyncServer IP
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "serverAddressEditTxt")).sendKeys(ip);

        // set SyncServer port
        if (!TextUtils.isEmpty(port)) {
            driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "serverPortEditTxt")).sendKeys(port);
        }

        // enable or disable SSL
        if (Boolean.parseBoolean(ssl)) {
            driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "sslChk")).click();
        }

        // save synchronization settings
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "save")).click();

        driver.findElement(By.id("android:id/button1")).click();


        driver.findElement(By.id("gr.tessera.easytec.omexom:id/clearCredentialsBtn")).click();


        driver.findElement(By.id("android:id/button1")).click();

        // we are the main menu
    }



    @When("^I will set my data to log in the application with (.*),(.*)$")
    public void I_will_set_my_data_to_log_in_the_application_with(String username, String password) throws Throwable {

        // click synchronization action bar button
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "sync")).click();

        // set username
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "usernameEditTxt")).sendKeys(username);
        // set password
        driver.findElement(By.id(AndroidSetup_OMEXON.app_package_name + ":id/" + "passwordEditTxt")).sendKeys(password);
        // synchronize (click OK)

        driver.findElement(By.id("android:id/button1")).click();


        //Thread.sleep(35000);
        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                driver.findElement(By.id("android:id/button2")).click();
            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }

        }

    }


    @Then("^I will click first activity item with text (.*) and click the order with id (.*)$")
    public void I_will_click_first_activity_item_with_text_Orders_and_click_the_order_with_id(String MenuOption,String orderNo) throws Throwable {
        // //*[contains(@text, 'Sign Out')]
        driver.findElement(By.xpath("//android.widget.TextView[@text='"+MenuOption+"']")).click();

        assert driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")) != null;
        if(driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")) != null){
            driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")).click();
        }else{

        }

    }




    @And("^I will find the seconds activity menu item (.*)$")
    public void I_will_find_the_seconds_activity_menu_item_Employees(String MenuOption) throws InterruptedException {


        if (driver.findElements(By.xpath("//android.widget.TextView[@text='" + MenuOption + "']")).size() != 0) {
            driver.findElement(By.xpath("//android.widget.TextView[@text='" + MenuOption + "']")).click();
        } else {
            throw new NullPointerException("The is none Description Object in Ui panel");
        }
    }



    @Then("^I will click on the dictionary icon and set the Texts$")
    public void I_will_click_on_the_dictionary_icon_and_set_the_Texts() throws InterruptedException {

        Thread.sleep(3000);

        driver.findElement(By.id("gr.tessera.easytec.omexom:id/dicBtn")).click();

        Thread.sleep(2000);

        driver.findElement(By.xpath("//android.widget.Button[contains(@text,'Task')]")).click();

        Thread.sleep(2000);


        StringBuilder.append(driver.findElement(By.className("android.widget.ListView")).findElements(By.className("android.widget.TextView")).get(0).getText());
        driver.findElement(By.className("android.widget.ListView")).findElements(By.className("android.widget.TextView")).get(0).click();
        StringBuilder.append(" ");

        Thread.sleep(2000);

        StringBuilder.append(driver.findElement(By.className("android.widget.ListView")).findElements(By.className("android.widget.TextView")).get(0).getText());
        driver.findElement(By.className("android.widget.ListView")).findElements(By.className("android.widget.TextView")).get(0).click();
        StringBuilder.append(" ");

        Thread.sleep(2000);

        StringBuilder.append(driver.findElement(By.className("android.widget.ListView")).findElements(By.className("android.widget.TextView")).get(0).getText());
        driver.findElement(By.className("android.widget.ListView")).findElements(By.className("android.widget.TextView")).get(0).click();
        StringBuilder.append(" ");

        driver.findElement(By.id("gr.tessera.easytec.omexom:id/action_ok")).click();

        Thread.sleep(2000);
    }




    @And("^I will check if the text is the same$")
    public void  I_will_check_if_the_text_is_the_same(){

        Assert.assertTrue(StringBuilder.toString().contains(driver.findElement(By.xpath("//android.widget.EditText[contains(@text,'Abgasmessung Boiler Erdgeschoss')]")).getText()));

    }

}
