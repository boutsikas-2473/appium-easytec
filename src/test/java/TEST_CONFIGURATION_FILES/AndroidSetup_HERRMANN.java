package TEST_CONFIGURATION_FILES;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by tesse on 5/3/2017.
 */
public class AndroidSetup_HERRMANN {



    private static AndroidDriver driver;

    public static final String app_package_name = "gr.tessera.easytec.herrmann";

    public static void deviceCapabilities() throws Exception {

        DesiredCapabilities capabilities = new DesiredCapabilities();

        // On Android the "deviceName" capability is currently ignored, though it remains required.
        capabilities.setCapability("deviceName", "emulator-23");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("setWebContentsDebuggingEnabled ",true);
        capabilities.setCapability("appPackage", app_package_name);
        capabilities.setCapability("appActivity", "gr.tessera.easytec.screens.MainActivity");
        capabilities.setCapability("testdroid_testTimeout", 25000);
        try {
            if (driver == null) {
                driver = new AndroidDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
                driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public static AndroidDriver getDriver() {
        return driver;
    }
    public static void tearDown() throws Exception {
        if (driver != null) {
            driver.quit();
        }

    }



}
