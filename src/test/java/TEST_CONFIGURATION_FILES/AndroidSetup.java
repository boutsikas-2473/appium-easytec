package TEST_CONFIGURATION_FILES;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public  class AndroidSetup {

    private static AndroidDriver driver = null;
    public static String MAIN_ACTIVITY=".screens.MainActivity";
   public static final String PACKAGE_NAME= "gr.tessera.easytec.ks";
   //public static final String PACKAGE_NAME= "gr.tessera.easytec.herrmann";
//    public static final String PACKAGE_NAME= "gr.tessera.easytec";
    public  static  DesiredCapabilities capabilities = new DesiredCapabilities();

    public static void deviceCapabilities() {
        // On Android the "deviceName" capability is currently ignored, though it remains required.
        capabilities.setCapability("deviceName", "emulator-23");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("setWebContentsDebuggingEnabled ",true);
        capabilities.setCapability("appPackage", PACKAGE_NAME);
        capabilities.setCapability("appActivity", "gr.tessera.easytec.screens.MainActivity");

    }

    public static AndroidDriver<MobileElement> getDriver()  {

        if(driver==null){
            deviceCapabilities();
            try {
                driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            return driver;
        }else{ return driver;}
    }



    public static void tearDown() throws Exception {
        if (driver != null) {
            driver.quit();
        }

    }

}