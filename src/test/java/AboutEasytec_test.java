import TEST_CONFIGURATION_FILES.AndroidSetup;
import TEST_CONFIGURATION_FILES.AndroidSetup_HERRMANN;
import TEST_CONFIGURATION_FILES.AndroidSetup_KS;
import TEST_CONFIGURATION_FILES.AndroidSetup_OMEXON;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.android.AndroidDriver;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by tesse on 5/10/2017.
 */
public class AboutEasytec_test {

    private static AndroidDriver driver;
    private static String Address  ="Wallersheimer Weg 50-58, 56070 Koblenz",Website="www.easytec-software.de",Tel="+49 261 98848-200",Fax="+49 261 98848-150",Email="easytec@easytec-software.de";


    @Before
    public void before() {
        try {

            AndroidSetup.deviceCapabilities();
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver = AndroidSetup.getDriver();


    }


    @When("^I go to the manu option AboutEasytec for (.*) flavor$")
    public void I_go_to_the_manu_option_AboutEasytec_flavor(String Menuoption){

        driver.findElement(By.xpath("//android.widget.TextView[@text='Settings']")).click();

        driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+Menuoption+"')]")).click();


    }


    @Then("^I will check the datafor (.*) Flavor$")
    public void I_will_check_the_datafor_Generic_Flavor(String Flavor1){

        WebElement ListView =  driver.findElement(By.className("android.widget.ListView"));
        List<WebElement> TwoLineListViewList = ListView.findElements(By.className("android.widget.TwoLineListItem"));


        // check Registration
           try{
           Assert.assertNotNull("The text doesnt exits",TwoLineListViewList.get(1).findElement(By.xpath("//android.widget.TextView[contains(@text,'EasyTec Software GmbH')]")));
            System.out.println(TwoLineListViewList.get(1).findElement(By.xpath("//android.widget.TextView[contains(@text,'EasyTec Software GmbH')]")).getText());

           //check for address
           Assert.assertNotNull("The text doesnt exist",TwoLineListViewList.get(2).findElement(By.xpath("//android.widget.TextView[@text='"+Address+"']")));
               System.out.println(TwoLineListViewList.get(2).findElement(By.xpath("//android.widget.TextView[@text='"+Address+"']")).getText());
           //check for Website
           Assert.assertNotNull("The text doesnt exist",TwoLineListViewList.get(3).findElement(By.xpath("//android.widget.TextView[@text='"+Website+"']")));
               System.out.println(TwoLineListViewList.get(3).findElement(By.xpath("//android.widget.TextView[@text='"+Website+"']")).getText());
           //Check telephone
            Assert.assertNotNull("The text doesnt exist",TwoLineListViewList.get(4).findElement(By.xpath("//android.widget.TextView[@text='"+Tel+"']")));
               System.out.println(TwoLineListViewList.get(4).findElement(By.xpath("//android.widget.TextView[@text='"+Tel+"']")).getText());
            //check Fax
            Assert.assertNotNull("The text doesnt exist",TwoLineListViewList.get(5).findElement(By.xpath("//android.widget.TextView[@text='"+Fax+"']")));
               System.out.println(TwoLineListViewList.get(5).findElement(By.xpath("//android.widget.TextView[@text='"+Fax+"']")).getText());
            //check Email
            Assert.assertNotNull("The text doesnt exist",TwoLineListViewList.get(6).findElement(By.xpath("//android.widget.TextView[@text='"+Email+"']")));
               System.out.println(TwoLineListViewList.get(6).findElement(By.xpath("//android.widget.TextView[@text='"+Email+"']")).getText());

           }catch(Exception e){

               Assert.assertFalse("One of the textviews doesnt exist",true);

           }



    }




}
