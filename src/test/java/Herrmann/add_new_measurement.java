package Herrmann;

import TEST_CONFIGURATION_FILES.AndroidSetup_HERRMANN;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import io.appium.java_client.MobileElement;
import io.appium.java_client.SwipeElementDirection;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import org.apache.http.util.TextUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by tesse on 5/5/2017.
 */
public class add_new_measurement {


    private static AndroidDriver driver;
    private String mDate=null,mMoisture=null,mTemperature=null,mAirSpeed=null,mDigits=null,mSKT=null;


    //******************* Setting the driver and the capabilities
    @Before
    public void before() {
        try {
            AndroidSetup_HERRMANN.deviceCapabilities();
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver = AndroidSetup_HERRMANN.getDriver();


    }



    @Given("^first user must log_in in the sync Server with (.*),(.*),(.*)$")
    public void first_user_must_log__in_in_the_sync_Server_with(String ip, String port, String ssl) {


        driver.findElement(By.xpath("//android.widget.TextView[@text='Settings']")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Account']")).click();

        // set SyncServer IP
        driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "serverAddressEditTxt")).sendKeys(ip);

        // set SyncServer port
        if (!TextUtils.isEmpty(port)) {
            driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "serverPortEditTxt")).sendKeys(port);
        }

        // enable or disable SSL
        if (Boolean.parseBoolean(ssl)) {
            driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "sslChk")).click();
        }

        // save synchronization settings
        driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "save")).click();

        driver.findElement(By.id("android:id/button1")).click();


        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/clearCredentialsBtn")).click();


        driver.findElement(By.id("android:id/button1")).click();

        // we are the main menu
    }



    @When("^The User must  successfully log-in in the system with (.*),(.*)$")
    public void The_User_must_has_successfully__log_in_in_the_system_with(String username, String password) throws Throwable {

        // click synchronization action bar button
        driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "sync")).click();

        // set username
        driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "usernameEditTxt")).sendKeys(username);
        // set password
        driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "passwordEditTxt")).sendKeys(password);
        // synchronize (click OK)

        driver.findElement(By.id("android:id/button1")).click();



        Thread.sleep(20000);
        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                driver.findElement(By.id("android:id/button2")).click();
            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }

        }

    }





    @And("^Find the menu button with name Order and press it$")
    public void Find_the_menu_button_with_name_Order_and_press_it() throws Throwable {
        // //*[contains(@text, 'Sign Out')]

        driver.findElement(By.xpath("//android.widget.TextView[@text='Orders']")).click();
    }


    @And("^I will close the Filter Panel and find the order with id (.*) and click it")
    public void I_will_close_the_Filter_Panel_and_find_the_order_with_id__and_click_it(String orderNo) throws Throwable {

        //close the filter panel
        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/filtersToggleTxt")).click();


        if(driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")) != null){
            driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")).click();
        }else{

        }

    }




       @And("^I will close the Briefing Panel Scroll down and click on the menu option Measurement Points$")
       public void I_will_close_the_Briefing_Panel_Scroll_down_and_click_on_the_menu_option_Measurement_Points() throws InterruptedException {


        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/briefingToggleTxt")).click();

        Thread.sleep(2000);

        WebElement RecyclerViewer = driver.findElement(By.className("android.support.v7.widget.RecyclerView"));


        TouchAction touchAction = new TouchAction(driver);


        touchAction.press(RecyclerViewer).moveTo(RecyclerViewer.getLocation().getX(),RecyclerViewer.getLocation().getY()-400).release();

        touchAction.perform();


        Thread.sleep(2000);

        driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'Measurement Points')]")).click();

        Thread.sleep(1000);

    }


    @And("^I will add a new Measurement point with proper values$")
    public void I_will_add_a_new_Measurement_point_with_proper_values() throws InterruptedException {

        //click on the add new Measurements
        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/add")).click();

        Thread.sleep(2500);

        driver.findElement(By.className("android.widget.EditText")).sendKeys("Test");


        Thread.sleep(1000);

        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/dicText")).sendKeys("A testing Measurement");

        Thread.sleep(2000);

        driver.hideKeyboard();


        Thread.sleep(1000);

        List<WebElement> Spinners = driver.findElements(By.id("gr.tessera.easytec.herrmann:id/valueSp"));


        Spinners.get(0).click();

        driver.findElement(By.xpath("//android.widget.CheckedTextView[contains(@text,'101 - Raumtemperatur')]")).click();

        Thread.sleep(1000);


        Spinners.get(1).click();


        driver.findElement(By.xpath("//android.widget.CheckedTextView[contains(@text,'Bad')]")).click();

        Thread.sleep(1000);

        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/save")).click();

    }

    @And("^I will save it and click it to set some needed informations and check on the finish$")
    public void I_will_save_it_and_click_it_to_set_some_needed_informations_and_check_on_the_finish() throws InterruptedException {

        driver.findElement(By.className("android.widget.ListView")).findElement(By.className("android.widget.LinearLayout")).click();

        //set date

        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/datePickerBtn")).click();

        Thread.sleep(1000);

        driver.findElement(By.id("android:id/button1")).click();

        //set mDate String
        mDate = driver.findElement(By.id("gr.tessera.easytec.herrmann:id/datePickerBtn")).getText();

        Thread.sleep(1000);

        List<WebElement> AmountTexts = driver.findElements(By.id("gr.tessera.easytec.herrmann:id/amountTxt"));

        //set moisture data

        AmountTexts.get(0).click();

        //set Moisture String Value
        mMoisture = "10";

        Thread.sleep(1500);

        driver.findElement(By.xpath("//android.widget.Button[@text='10']")).click();

        Thread.sleep(1000);

        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/okBtn")).click();

        Thread.sleep(1000);

        //set temperature value

        AmountTexts.get(1).click();

        Thread.sleep(1500);

        driver.findElement(By.xpath("//android.widget.Button[@text='10']")).click();


        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/okBtn")).click();

        mTemperature = "10";

        Thread.sleep(1000);

        //set air speed

        AmountTexts.get(2).click();

        Thread.sleep(1500);

        driver.findElement(By.xpath("//android.widget.Button[@text='10']")).click();

        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/okBtn")).click();

        mAirSpeed ="10";

        Thread.sleep(1000);

        //set Digits

        AmountTexts.get(3).click();

        Thread.sleep(1500);

        driver.findElement(By.xpath("//android.widget.Button[@text='10']")).click();

        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/okBtn")).click();

        mDigits="10";

        Thread.sleep(1000);

        //set SKT value

        AmountTexts.get(4).click();

        Thread.sleep(1500);

        driver.findElement(By.xpath("//android.widget.Button[@text='10']")).click();

        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/okBtn")).click();

        mSKT = "10";

        Thread.sleep(1000);

        //click on the Finished CheckBox

        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/valueChk")).click();

        Thread.sleep(1000);

        //save

        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/save")).click();



    }


    @And("^I will sync and check if the lock icon exist and the values are the same (.*)$")
    public void I_will_sync_and_check_if_the_lock_icon_exist_and_the_values_are_the_same(String orderNo) throws InterruptedException {

        driver.findElement(By.id("android:id/home")).click();

        Thread.sleep(2000);

        driver.findElement(By.id("android:id/home")).click();

        Thread.sleep(2000);

        driver.findElement(By.id("android:id/home")).click();

        Thread.sleep(2000);


        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/sync")).click();

        Thread.sleep(20000);


        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                driver.findElement(By.id("android:id/button2")).click();
            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }

        }


        Thread.sleep(1000);

        driver.findElement(By.xpath("//android.widget.TextView[@text='Orders']")).click();



        //close the filter panel
        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/filtersToggleTxt")).click();


        if(driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")) != null){
            driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")).click();
        }



        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/briefingToggleTxt")).click();

        Thread.sleep(2000);

        WebElement RecyclerViewer = driver.findElement(By.className("android.support.v7.widget.RecyclerView"));


        TouchAction touchAction = new TouchAction(driver);


        touchAction.press(RecyclerViewer).moveTo(RecyclerViewer.getLocation().getX(),RecyclerViewer.getLocation().getY()-400).release();

        touchAction.perform();


        Thread.sleep(2000);

        driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'Measurement Points')]")).click();

        Thread.sleep(1000);


        //************check for the Lock Icon
        try{
            Assert.assertNotNull("The Lock Icon doesnt exist",driver.findElement(By.id("gr.tessera.easytec.herrmann:id/lockedImg")).isDisplayed());
        }catch(Exception e){
            Assert.assertTrue("The is no Lock Icon",true);
        }

        //******************

        //************check for the Flag  Icon
        try{
            Assert.assertNotNull("The Finish Icon doesnt exist",driver.findElement(By.id("gr.tessera.easytec.herrmann:id/finishedImg")).isDisplayed());
        }catch(Exception e){
            Assert.assertTrue("The is no Finish Icon",true);
        }


        //******************


        driver.findElement(By.className("android.widget.ListView")).findElement(By.className("android.widget.LinearLayout")).click();

        Thread.sleep(2000);


        Assert.assertTrue("The Dates are not equals",driver.findElement(By.id("gr.tessera.easytec.herrmann:id/historyMeasurementDateTxt")).getText().contains(mDate));
        Assert.assertTrue(driver.findElement(By.id("gr.tessera.easytec.herrmann:id/historyEmployeeTxt")).getText().contains("Krumscheid"));
        Assert.assertTrue(driver.findElement(By.id("gr.tessera.easytec.herrmann:id/historyMeasurementMoistureTxt")).getText().contains(mMoisture));
        Assert.assertTrue(driver.findElement(By.id("gr.tessera.easytec.herrmann:id/historyMeasurementTemperatureTxt")).getText().contains(mTemperature));
        Assert.assertTrue(driver.findElement(By.id("gr.tessera.easytec.herrmann:id/historyMeasurementAirSpeedTxt")).getText().contains(mAirSpeed));
        Assert.assertTrue(driver.findElement(By.id("gr.tessera.easytec.herrmann:id/historyMeasurementDigitsTxt")).getText().contains(mDigits));
        Assert.assertTrue(driver.findElement(By.id("gr.tessera.easytec.herrmann:id/historyMeasurementSKTTxt")).getText().contains(mSKT));

        Thread.sleep(2000);


    }




}
