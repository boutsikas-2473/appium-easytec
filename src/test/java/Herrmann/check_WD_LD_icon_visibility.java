package Herrmann;

import TEST_CONFIGURATION_FILES.AndroidSetup;
import TEST_CONFIGURATION_FILES.AndroidSetup_HERRMANN;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.lexer.Pl;
import io.appium.java_client.android.AndroidDriver;
import org.apache.http.util.TextUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by tesse on 5/3/2017.
 */
public class check_WD_LD_icon_visibility {



    private static AndroidDriver driver;
    private int position =0,counter=0;
    private boolean found=false;
    private WebElement LinearLayout=null,LeadDetectionReport=null,Places=null;

    //******************* Setting the driver and the capabilities
    @Before
    public void before() {
        try {
            AndroidSetup_HERRMANN.deviceCapabilities();
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver = AndroidSetup_HERRMANN.getDriver();


    }



    @Given("^First user must log-in in the sync Server with (.*),(.*),(.*)$")
    public void First_user_must_log__in_in_the_sync_Server_with(String ip, String port, String ssl) {


        driver.findElement(By.xpath("//android.widget.TextView[@text='Settings']")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Account']")).click();

        // set SyncServer IP
        driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "serverAddressEditTxt")).sendKeys(ip);

        // set SyncServer port
        if (!TextUtils.isEmpty(port)) {
            driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "serverPortEditTxt")).sendKeys(port);
        }

        // enable or disable SSL
        if (Boolean.parseBoolean(ssl)) {
            driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "sslChk")).click();
        }

        // save synchronization settings
        driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "save")).click();

        driver.findElement(By.id("android:id/button1")).click();


        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/clearCredentialsBtn")).click();


        driver.findElement(By.id("android:id/button1")).click();

        // we are the main menu
    }



    @When("^The User has successfully log-in in the system with (.*),(.*)$")
    public void The_User_has_successfully__log_in_in_the_system_with(String username, String password) throws Throwable {

        // click synchronization action bar button
        driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "sync")).click();

        // set username
        driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "usernameEditTxt")).sendKeys(username);
        // set password
        driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "passwordEditTxt")).sendKeys(password);
        // synchronize (click OK)

        driver.findElement(By.id("android:id/button1")).click();


        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                driver.findElement(By.id("android:id/button2")).click();
            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }

        }

    }





    @And("^Find the menu button with name Order and click it$")
    public void Find_the_menu_button_with_name_Order_and_click_it() throws Throwable {
        // //*[contains(@text, 'Sign Out')]

        driver.findElement(By.xpath("//android.widget.TextView[@text='Orders']")).click();
    }


    @And("^I will find the order with id (.*)")
    public void I_will_find_a_complete_order_with_id(String orderNo) throws Throwable {

        //close the filter panel
        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/filtersToggleTxt")).click();


        WebElement ListView = driver.findElement(By.className("android.widget.ListView"));
        List<WebElement> LinearLayoutList = ListView.findElements(By.className("android.widget.LinearLayout"));


        do{
                    if(LinearLayoutList.get(counter).findElement(By.id("gr.tessera.easytec.herrmann:id/orderIdTxt")).getText().contains(orderNo)){
                        LinearLayout = LinearLayoutList.get(counter);
                        position=counter;
                        found=!found;
                        counter++;
                    }else{
                            counter++;
                    }
        }while(found==false || LinearLayoutList.size()==counter);







    }


    @Then("^I will check if the icon with id (.*) exist$")
    public void I_will_check_if_the_icon_with_id_WD_exist(String Icon_id) throws InterruptedException {

      Thread.sleep(2000);


            LinearLayout.click();

            Thread.sleep(2000);

            driver.findElement(By.id("gr.tessera.easytec.herrmann:id/briefingToggleTxt")).click();

            Thread.sleep(2000);



            try{
                Places = driver.findElement(By.xpath("//android.widget.TextView[@text='Places']"));
                System.out.println(Places.getText());
                LeadDetectionReport = driver.findElement(By.xpath("//android.widget.TextView[@text='Leak Detection Report']"));
            }catch(Exception e){
                LeadDetectionReport=null;
                    if(Icon_id.matches("WD")){
                        Places = driver.findElement(By.xpath("//android.widget.TextView[@text='Places']"));
                    }else{
                        Places=null;
                    }


            }


            Thread.sleep(2000);

            driver.findElement(By.id("android:id/home")).click();


                if(LeadDetectionReport==null&&Places!=null){
                    Assert.assertNotNull("The icon doesnt exist",LinearLayout.findElement(By.id("gr.tessera.easytec.herrmann:id/orderTypeIcon")));
                }else if(LeadDetectionReport!=null && Places!=null){
                    Assert.assertNotNull("The icon doesnt exist",LinearLayout.findElement(By.id("gr.tessera.easytec.herrmann:id/orderTypeIcon")));
                }else{

                    try{
                        Assert.assertNotNull("The icon doesnt exist",LinearLayout.findElement(By.id("gr.tessera.easytec.herrmann:id/orderTypeIcon")));
                    }catch(Exception e){
                        Assert.assertTrue("The icon doesnt exist",true);
                    }

                }



    }



    @After
    public void after() {
        try {
            driver.resetApp();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    @AfterClass
    public static void teardown() throws Exception {
        //driver.quit();
        AndroidSetup.tearDown();
        System.out.println("Ran the after");
    }








}
