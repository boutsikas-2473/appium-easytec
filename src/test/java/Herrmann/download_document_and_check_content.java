package Herrmann;

import TEST_CONFIGURATION_FILES.AndroidSetup_HERRMANN;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.lexer.Th;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.apache.http.util.TextUtils;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.sql.Time;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by tesse on 5/9/2017.
 */


public class download_document_and_check_content {

    private static AndroidDriver driver;
    private static String OS = "android";
    private static String EYE_PATH = "//"+OS+".view.View/"+OS+".widget.FrameLayout[0]/"+OS+".view.View[0]/"+OS+".widget.LinearLayout[1]/"+OS+".widget.TextView";

    //******************* Setting the driver and the capabilities
    @Before
    public void before() {
        try {
            AndroidSetup_HERRMANN.deviceCapabilities();
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver = AndroidSetup_HERRMANN.getDriver();



    }



    @Given("^I will find the path and signin the SyncSServer with (.*),(.*),(.*)$")
    public void I_will_find_the_path_and_signin_theSyncSServer_with(String ip, String port, String ssl) {


        driver.findElement(By.xpath("//"+OS+".widget.TextView[@text='Settings']")).click();

        driver.findElement(By.xpath("//"+OS+".widget.TextView[@text='Account']")).click();

        // set SyncServer IP
        driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "serverAddressEditTxt")).sendKeys(ip);

        // set SyncServer port
        if (!TextUtils.isEmpty(port)) {
            driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "serverPortEditTxt")).sendKeys(port);
        }

        // enable or disable SSL
        if (Boolean.parseBoolean(ssl)) {
            driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "sslChk")).click();
        }

        // save synchronization settings
        driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "save")).click();

        driver.findElement(By.id(OS+":id/button1")).click();


        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/clearCredentialsBtn")).click();


        driver.findElement(By.id(OS+":id/button1")).click();

        // we are the main menu
    }



    @When("^I go back and sign in as a valid User with (.*),(.*)$")
    public void I_go_back_and_sign_in_as_a_valid_User_with(String username, String password) throws Throwable {

        // click synchronization action bar button
        driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "sync")).click();

        // set username
        driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "usernameEditTxt")).sendKeys(username);
        // set password
        driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "passwordEditTxt")).sendKeys(password);
        // synchronize (click OK)

        driver.findElement(By.id(OS+":id/button1")).click();

        Thread.sleep(40000);

        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                TouchAction touchAction = new TouchAction(driver);

                touchAction.press( driver.findElement(By.id("android:id/button1"))).release().perform();




            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }

        }

    }


    @And("^I click to download the documents and find the order with id (.*)$")
    public void I_click_to_download_the_documents_and_find_the_order_with_id(String orderNo) throws Throwable {
        // //*[contains(@text, 'Sign Out')]

        driver.manage().timeouts().implicitlyWait(150,TimeUnit.SECONDS);
        driver.findElement(By.id(OS+":id/button1")).click();

        driver.findElement(By.xpath("//"+OS+".widget.TextView[@text='Orders']")).click();


        if(driver.findElement(By.xpath("//"+OS+".widget.TextView[contains(@text,'"+orderNo+"')]")) != null){
            driver.findElement(By.xpath("//"+OS+".widget.TextView[contains(@text,'"+orderNo+"')]")).click();
        }
    }



    @And("^I will click on the menu item (.*)$")
    public void I_will_click_on_the_menu_item(String MenuOption) throws InterruptedException {

        Thread.sleep(2000);

        driver.findElement(By.xpath("//android.widget.TextView[@text='Documents']")).click();
      //  driver.findElement(By.xpath("//"+OS+".widget.TextView[contains(@text,'" + MenuOption + "')]"));


        Thread.sleep(2000);

    }



    @Then("^I will check in tow documents if it shows a pop up and if they have content$")
    public void I_will_check_in_tow_documents_if_it_shows_a_pop_up__and_if_they_have_content() throws InterruptedException {


        WebElement ListView = driver.findElement(By.className(OS + ".widget.ListView"));
        List<WebElement> LinearLayoutList = ListView.findElements(By.className(OS + ".widget.LinearLayout"));


        //start form the first order

        LinearLayoutList.get(0).click();

        //first check if the pop up is been activated


        Thread.sleep(2000);

        // find the "eye" icon
        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/viewDocument")).click();


        try {
            Thread.sleep(5000);
            driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'This document is missing. Would you like')]")).isDisplayed();
        } catch (Exception e) {

            try {
                Assert.assertNotNull("The object doesnt exist", driver.findElement(By.className("android.support.v4.view.ViewPager")));

            } catch (Exception ex) {
                Assert.assertTrue("The Object throw Exception", false);

            }


        }

    }



    }













