package Herrmann;

import TEST_CONFIGURATION_FILES.AndroidSetup_HERRMANN;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.lexer.Th;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import org.apache.http.util.TextUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.List;

/**
 * Created by tesse on 5/11/2017.
 */
public class check_ground_plan {

    private static AndroidDriver driver;
    private String mDate=null,mMoisture=null,mTemperature=null,mAirSpeed=null,mDigits=null,mSKT=null;
    private int X=0,Y=0;


    //******************* Setting the driver and the capabilities
    @Before
    public void before() {
        try {
            AndroidSetup_HERRMANN.deviceCapabilities();
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver = AndroidSetup_HERRMANN.getDriver();


    }



    @Given("^when I will sign in the Server with (.*),(.*),(.*)$")
    public void when_I_will_sign_in_the_Server_with(String ip, String port, String ssl) {


        driver.findElement(By.xpath("//android.widget.TextView[@text='Settings']")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Account']")).click();

        // set SyncServer IP
        driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "serverAddressEditTxt")).sendKeys(ip);

        // set SyncServer port
        if (!TextUtils.isEmpty(port)) {
            driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "serverPortEditTxt")).sendKeys(port);
        }

        // enable or disable SSL
        if (Boolean.parseBoolean(ssl)) {
            driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "sslChk")).click();
        }

        // save synchronization settings
        driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "save")).click();

        driver.findElement(By.id("android:id/button1")).click();


        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/clearCredentialsBtn")).click();


        driver.findElement(By.id("android:id/button1")).click();

        // we are the main menu
    }



    @When("^I will sign-in as a valid user with (.*),(.*) and download the documents$")
    public void I_will_sign_in_as_a_valid_user_with_and_download_the_documents(String username, String password) throws Throwable {

        // click synchronization action bar button
        driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "sync")).click();

        // set username
        driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "usernameEditTxt")).sendKeys(username);
        // set password
        driver.findElement(By.id(AndroidSetup_HERRMANN.app_package_name + ":id/" + "passwordEditTxt")).sendKeys(password);
        // synchronize (click OK)

        driver.findElement(By.id("android:id/button1")).click();



        Thread.sleep(30000);
        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                driver.findElement(By.id("android:id/button2")).click();
            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }

        }

    }





    @And("^I will Find the Orrder with id (.*) in the menu$")
    public void I_will_find_the_order_with_idinthemenu(String orderNo) throws Throwable {
        // //*[contains(@text, 'Sign Out')]

        driver.findElement(By.xpath("//android.widget.TextView[@text='Orders']")).click();
        //close the filter panel
        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/filtersToggleTxt")).click();


        if(driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")) != null){
            driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")).click();
        }else{

        }

    }




    @And("^I will find the Menu option (.*) and add and save a new Place with Name (.*) and groundPlan$")
    public void I_will_close_the_Briefing_Panel_Scroll_down_and_click_on_the_menu_option_Measurement_Points(String MenuOption,String PlaceName) throws InterruptedException {


        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/briefingToggleTxt")).click();

        Thread.sleep(2000);

        WebElement RecyclerViewer = driver.findElement(By.className("android.support.v7.widget.RecyclerView"));


        TouchAction touchAction = new TouchAction(driver);


        touchAction.press(RecyclerViewer).moveTo(RecyclerViewer.getLocation().getX(),RecyclerViewer.getLocation().getY()-400).release();

        touchAction.perform();


        Thread.sleep(2000);

        driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+MenuOption+"')]")).click();

        Thread.sleep(1000);


        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/add")).click();

        WebElement ListView = driver.findElement(By.className("android.widget.ListView"));
        List<WebElement>  LinearLayout = ListView.findElements(By.className("android.widget.LinearLayout"));

        //go to linear layout at position 0 and set the name
            LinearLayout.get(0).findElement(By.className("android.widget.EditText")).sendKeys(PlaceName);

            Thread.sleep(1000);

            //set the Spinner
            LinearLayout.get(2).findElement(By.className("android.widget.Spinner")).click();
            Thread.sleep(1000);
            driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'doc_I6K8P6C7HKH5.jpg')]")).click();

            Thread.sleep(1000);

            // click on the save
            driver.findElement(By.id("gr.tessera.easytec.herrmann:id/save")).click();


    }


    @And("^I will go back and i will add a new (.*) using the place above with name (.*)$")
    public void I_will_go_back_and_i_will_add_a_new_Measurement_using_the_place_above_with_name_newplace(String MenuOption,String placeOption) throws InterruptedException {


        driver.findElement(By.id("android:id/home")).click();

        Thread.sleep(2000);

        WebElement RecyclerViewer = driver.findElement(By.className("android.support.v7.widget.RecyclerView"));


        TouchAction touchAction = new TouchAction(driver);


        touchAction.press(RecyclerViewer).moveTo(RecyclerViewer.getLocation().getX(),RecyclerViewer.getLocation().getY()-400).release();

        touchAction.perform();


        Thread.sleep(2000);

        driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+MenuOption+"')]")).click();

        Thread.sleep(1000);



        // find the WebElement Linear


        //click on the spinner
        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/placeSp")).click();
        Thread.sleep(2000);

        driver.findElement(By.xpath("//android.widget.CheckedTextView[contains(@text,'newplace')]")).click();

        Thread.sleep(1000);

        //click on the ground button
        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/ground_plan")).click();

        Thread.sleep(2000);


        driver.findElement(By.id("android:id/button1")).click();

        Thread.sleep(3000);

        driver.findElement(By.id("android:id/button1")).click();

        Thread.sleep(1000);

        //click on the ground button
        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/ground_plan")).click();

        Thread.sleep(1000);

        WebElement StartElement = driver.findElement(By.xpath("//android.widget.TextView[@text='MP6']"));


        TouchAction touchActionBack = new TouchAction((MobileDriver)driver).tap(StartElement,StartElement.getLocation().x+500,StartElement.getLocation().y+200).waitAction();


        touchActionBack.perform();

        Thread.sleep(5000);

        X = StartElement.getLocation().x;
        Y  = StartElement.getLocation().y;


    }




    @Then("^I will check if the textview changes position$")
    public void I_will_check_if_the_textview_change_position() throws InterruptedException {


        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/save")).click();

        Thread.sleep(1500);

        driver.findElement(By.id("android:id/home")).click();

        Thread.sleep(2000);



        //click on the ground button
        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/ground_plan")).click();


        Thread.sleep(9000);

        int TextViewX= driver.findElement(By.xpath("//android.widget.TextView[@text='MP6']")).getLocation().x;
        int TextViewY = driver.findElement(By.xpath("//android.widget.TextView[@text='MP6']")).getLocation().y;


        System.out.println(String.valueOf(X)+" >= "+String.valueOf(TextViewX));
        System.out.println(String.valueOf(Y)+" >= "+String.valueOf(TextViewY));
        Assert.assertTrue(X==TextViewX);
        Assert.assertTrue(Y==TextViewY);



    }

}
