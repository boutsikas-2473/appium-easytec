package Herrmann;

import TEST_CONFIGURATION_FILES.AndroidSetup;
import TEST_CONFIGURATION_FILES.AndroidSetup_HERRMANN;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.android.AndroidDriver;
import org.apache.http.util.TextUtils;
import org.junit.AfterClass;
import org.openqa.selenium.By;
import org.testng.Assert;

/**
 * Created by tesse on 5/8/2017.
 */
public class check_customer_icon_exist {


    private static AndroidDriver driver;
    private static String ICONS_PATH = "//android.widget.ScrollView/android.widget.LinearLayout[0]/";

    //******************* Setting the driver and the capabilities
    @Before
    public void before() {
        try {
            AndroidSetup_HERRMANN.deviceCapabilities();
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver = AndroidSetup_HERRMANN.getDriver();


    }



    @Given("^I first  must log_in in the sync Server with (.*),(.*),(.*)$")
    public void I_first_must_log__in_in_the_sync_Server_with(String ip, String port, String ssl) {


        driver.findElement(By.xpath("//android.widget.TextView[@text='Settings']")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Account']")).click();

        // set SyncServer IPAndroidSetup
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "serverAddressEditTxt")).sendKeys(ip);

        // set SyncServer port
        if (!TextUtils.isEmpty(port)) {
            driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "serverPortEditTxt")).sendKeys(port);
        }

        // enable or disable SSL
        if (Boolean.parseBoolean(ssl)) {
            driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "sslChk")).click();
        }

        // save synchronization settings
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "save")).click();

        driver.findElement(By.id("android:id/button1")).click();


        driver.findElement(By.id("gr.tessera.easytec.herrmann:id/clearCredentialsBtn")).click();


        driver.findElement(By.id("android:id/button1")).click();

        // we are the main menu
    }



    @When("^I have to  successfully log-in in the system with (.*),(.*)$")
    public void I_have_to_successfully_log__in_in_the_system_with(String username, String password) throws Throwable {

        // click synchronization action bar button
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "sync")).click();

        // set username
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "usernameEditTxt")).sendKeys(username);
        // set password
        driver.findElement(By.id(AndroidSetup.PACKAGE_NAME + ":id/" + "passwordEditTxt")).sendKeys(password);
        // synchronize (click OK)

        driver.findElement(By.id("android:id/button1")).click();



        Thread.sleep(30000);
        if (driver.findElement(By.className("android.widget.FrameLayout")).isDisplayed()) {


            if (driver.findElements(By.xpath("//android.widget.Button[@text='Later']")).size() != 0) {
                driver.findElement(By.id("android:id/button2")).click();
            } else {
                driver.findElement(By.id("android:id/button1")).click();

            }

        }

    }





    @And("^I will find the menu button with name Order and press it$")
    public void I_will_find_the_menu_button_with_name_Order_and_press_it() throws Throwable {
        // //*[contains(@text, 'Sign Out')]

        driver.findElement(By.xpath("//android.widget.TextView[@text='Orders']")).click();
    }

    @And("^I will find the order with  id (.*) and click it$")
    public void I_will_find_the_order_with_id_and_Click_it(String orderNo){


        if(driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")) != null){
            driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+orderNo+"')]")).click();
        }
    }

    @And("^I will find the menu option with name Customer$")
    public void I_will_find_the_menu_option_with_name_Customer() throws InterruptedException {

        Thread.sleep(2000);

        driver.findElement(By.xpath("//android.support.v7.widget.RecyclerView/android.widget.RelativeLayout[2]/android.widget.TextView")).click();

        Thread.sleep(2000);
    }

    @Then("^I will check if the icons exist$")
    public void I_will_check_if_the_icons_exist() throws InterruptedException {
        //the first button (the world button)
        Thread.sleep(2000);


        Assert.assertTrue(driver.findElement(By.xpath("//android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]")).findElements(By.className("android.widget.ImageButton")).get(0).isDisplayed(),"Is not Displayed");
        Assert.assertTrue(driver.findElement(By.xpath("//android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]")).findElements(By.className("android.widget.ImageButton")).get(1).isDisplayed(),"Is not Displayed");

    }


    @AfterClass
    public static void teardown() throws Exception {
        //driver.quit();
        AndroidSetup.tearDown();
        System.out.println("Ran the after");
    }







}
