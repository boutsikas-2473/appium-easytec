Feature: Check about easytec setting option if has all the porper data

    Background:
      Given I am logged in as "Frank Krumscheid"

    Scenario:
      When Go to Settings
      Then Go to About Easytec
      And  Version is 1.11.10, Registration is EasyTec Software GmbH, Address is Wallersheimer Weg 50-58, 56070 Koblenz, Website is www.easytec-software.de, Tel is 49 261 98848-200, Email is easytec@easytec-software.de, Owner is Frank Krumscheid
