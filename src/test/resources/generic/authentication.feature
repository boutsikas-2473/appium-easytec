Feature: aake the connection in order to run all the other features


  Scenario: Wrong version message
    When Navigate to main menu
    When Go to Settings
    Then Go to Account
    And  Set The SyncServer Credentials 192.168.0.133,4012,false
    And  Click Clear account data button
    And  Set the user  credentials fkrumscheid,123456
    Then Display error id Sync error


  Scenario: Make the connection
    When Navigate to main menu
    When Go to Settings
    Then Go to Account
    And  Set The SyncServer Credentials 192.168.0.133,4011,false
    And  Click Clear account data button
    And  Set the user data credentials fkrumscheid,123456


Scenario: Check application version
    When Navigate to main menu
    Then Go to Settings
    And  Go to About Easytec
    Then check if version is 1.11.10
    And  check if the version is not 1.12.10


