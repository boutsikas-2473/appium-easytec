Feature: Order document

  Scenario: Sort materials by Document Name in ascending order
    When Navigate to main menu
    And  Navigate to order ET-1002
    And  Go to Documents
    And  Sort  by Document Name in ascending order
    Then Materials are sorted by Document Name in ascending order

  Scenario: Sort materials by Document Name in descending order
    When Navigate to main menu
    And  Navigate to order ET-1002
    And  Go to Documents
    And  Sort  by Document Name in descending order
    Then Materials are sorted by Document Name in descending order

  Scenario: Sort materials by Date in ascending order
    When Navigate to main menu
    And  Navigate to order ET-1002
    And  Go to Documents
    And  Sort  by Date in ascending order
    Then Materials are sorted by Date in ascending order

  Scenario: Sort materials by Date in descending order
    When Navigate to main menu
    And  Navigate to order ET-1002
    And  Go to Documents
    And  Sort  by Date in descending order
    Then Materials are sorted by Date in descending order

  Scenario: Sort materials by Type in ascending order
    When Navigate to main menu
    And  Navigate to order ET-1002
    And  Go to Documents
    And  Sort  by Type in ascending order
    Then Materials are sorted by Type in ascending order

  Scenario: Sort materials by Type in descending order
    When Navigate to main menu
    And  Navigate to order ET-1002
    And  Go to Documents
    And  Sort  by Type in descending order
    Then Materials are sorted by Type in descending order

    Scenario: Check document icon visibility on a order
      When Navigate to main menu
      And  Navigate to Orders
      Then Check if the document exist in order ET-1002
      Then Check if the document not exist in order ET-1001