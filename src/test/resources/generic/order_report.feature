Feature: Order report

  Background:
    Given I am logged in as "Frank Krumscheid"


  Scenario: The report of an order that has not been completed cannot be emailed

    When Navigate to order ET-1002
    And  Go to Report (unfinished)
    Then Email icon is not visible


  Scenario: The report of an order that has been completed can be emailed by the main participant

    When Navigate to order ET-1003
    And  Go to Order Information
    And  The Krumscheid Frank is Main participant
    And  Navigate to main menu
    And  Navigate to order ET-1003
    And  Go to Report
    Then Email the report for email example@tessera.gr
