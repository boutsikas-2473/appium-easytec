Feature: Order

  Background:
    Given I am logged in as "Frank Krumscheid"

  Scenario: Delete an order
    When Navigate to main menu
    And  Navigate to Orders
    And  Long tap on order ET-1002
    And  Tap the delete button
    And  Tap on the OK button
    Then Order ET-1002 is not in the order list

  Scenario:  Check if after click on one order if checklist set value works and the order finished

    When Navigate to main menu
    And  Navigate to order ET-1001
    And  Go to Checklists/Signatures
    And  Click the venenatis lacinia aenean sit (10000)
    And  Click the 2 (6956)
    And  Set the value 10
    And  Save
    And  Click the 1 (1338)
    And  Tick the value
    And  Save
    And  Click the 3 (9211)
    And  Set Date value 20/06/2017
    And  Save
    And  Click the 5 (9912)
    And  Tick the value
    And  Save
    And  Click  the signatures
    And  Set the Signatures
    And  Save
    And  Navigate to main menu
    And  Navigate to order ET-1001
    And  Go to Employees
    And  Check  employee F. Krumscheid
    And  Set participant Start Date 07/06/2017 and Time 10:00
    And  Set participant Finish Date to 10/06/2017 and Time 11:00
    And  Set info This is the message
    And  Set executed text This is the executed text
    And  Save
    And  Set employees D. Ohlig
    And  Set participant Start Date 07/06/2017 and Time 10:00
    And  Set participant Finish Date to 10/06/2017 and Time 11:00
    And  Set info This is the message
    And  Set executed text This is the executed text
    And  Give participant employee signature
    And  Save
    And  Set employees F. Krumscheid
    And  Give employee signature
    And  Save
    And  Go back
    And  Go to Checklists/Signatures
    And  Click finalize flag
    And  Set order state neuer Termin
    And  Save
    And  Confirm the finalize of the order
    And  Navigate to main menu
    And  Synchronize
    And  Navigate to Orders
    Then Check if in the order ET-1001 flag exist



    Scenario: Sort materials by Start Date in ascending order
      Given Navigate to main menu
      Then  Navigate to Orders
      And   Sort  by Start Date in ascending order
      Then  Check order sorted by Start Date in ascending order

    Scenario: Sort materials by Start Date in descending order
      Given Navigate to main menu
      Then  Navigate to Orders
      And   Sort  by Start Date in descending order
      Then  Check order sorted by Start Date in descending order

    Scenario: Sort materials by Address in ascending order
      Given Navigate to main menu
      Then  Navigate to Orders
      And   Sort  by Address in ascending order
      Then  Check order sorted by Address in ascending order

    Scenario: Sort materials by Address in descending order
      Given Navigate to main menu
      Then  Navigate to Orders
      And   Sort  by Address in descending order
      Then  Check order sorted by Address in descending order

    Scenario: Sort materials by Name in ascending order
      Given Navigate to main menu
      Then  Navigate to Orders
      And   Sort  by Name in ascending order
      Then  Check order sorted by Name in ascending order

    Scenario: Sort materials by Name in descending order
      Given Navigate to main menu
      Then  Navigate to Orders
      And   Sort  by Name in descending order
      Then  Check order sorted by Name in descending order

    Scenario: Sort materials by Order No in ascending order
      Given Navigate to main menu
      Then  Navigate to Orders
      And   Sort  by Order No in ascending order
      Then  Check order sorted by Order No in ascending order

    Scenario: Sort materials by Order No in descending order
     Given Navigate to main menu
     Then  Navigate to Orders
     And   Sort  by Order No in descending order
     Then  Check order sorted by Order No in descending order



    Scenario: Order Menu Drag n Drop
      Given   Navigate to main menu
      When    Navigate to order ET-1001
      Then    Check Drag n Drop event


  Scenario: Check if user password exist after clearing account data
    Given  Navigate to main menu
    When   Go to Settings
    Then   Go to Account
    And    Click Clear account data button
    And    Sync
    Then   Check if the text is fkrumscheid
    And    Check if the password is empty




