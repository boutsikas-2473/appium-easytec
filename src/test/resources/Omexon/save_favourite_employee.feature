@testing
Feature: save_favourite_employee test


  Scenario Outline:  Check after login if favorite feature works

    Given  First user must log-in in the Server <ip>,<port>,<ssl>
    When   The user has been successfully sign-in as a valid user with <user>,<password>
    And    I press the Employee tab in the first menu list
    And    I long press the employee which i want single or multiple
    And    Click on the favorite button and I will check if the employeer has been saved in the favorite list
    And    I will go and add a new order with order <order> and click on the Hours and Cost button
    Then   I will check if in the Employee Button and  click to add a new Team Hour






    Examples:
      | ip | port | ssl | user | password |  order |
      |62.225.181.203 | 3403 | false | fkrumscheid | 123456 | 1030-005 |